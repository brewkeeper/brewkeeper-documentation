have a page that shows typical workflows for breweries, inclusive of set up and diagrams
- A wholesale location, and you allocate to orders and fulfill 
- Fulfill to Tap Room (with a POS partition)


# Sun 31st March

# main test that the problem is fixed
- stock younger lot
- allocate order1 with JAN (for younger lot)
- stock older lot (have to make sure to do this AFTER the above allocation)
- allocate order2 with NUB (for an older lot)
- fulfill order1 (old behaviour: takes the older lot, new behaviour: takes the younger lot)
- fulfill order2 (old behaviour: failed, new behaviour: succeeds)

# test that reverts are correctly handled
- stock lot1
- allocate order with JAN (lot1)
- revert allocation
- revert stock
- stock lot2
- allocate order with JAN (lot2)
- fulfill (this should only try to fulfill inventory from lot2)

a few other questions I had:
- is it possible to scan the same JAN code multiple times on the same scan event?
- is it possible for a NUB for a lot not to exist? because this code depends on the NUB existing


# Friday ...
- [X] mon. Call SOLT today
  possibly meet with Mick next Tuesday
- [X] wed. SOLT issue - manually fix orders for now (until we figure out a fix)
fulfillment location changed from 205a railway road -> kti canningvale storage for SGGS-C216 the following orders:
#4923 x3, #4920 x1, #4907 x1, #4908 x1, #4909 x1, #4899 x6
total: 13

- Fulfill allocated order shipment from Shopify

- [X] great dane - cannot see dn 61 on the billing account 'delivery notes' page
  issue is: we are iterating over customer ds, and this is an account dn
- [X] new rules for documents
  - dns and dnis ONLY for customers
  - invoices for customers OR billing accounts
- [ ] add pickup and delivery to carrierbot for great dane
- [X] 3x dns need to be dnis
  Apartment::Tenant.switch!('great-dane')
  dns = Document.where(id: [15, 40, 53])
  dns.map(&:name)

- [X] add speedo's translations for 'copy document' etc.
- [ ] allow showing billing account > delivery notes
- [x] look at all breweries - are there ANY dns / dnis linked to accounts directly and not customers?

dns = Document.any_delivery_notes.where(customer_id: nil)
pp dns.map { |dn| [dn.id, dn.name, dn.accounting_contact.customers.count] }

Tworabbits

[[3981, "DN-03981", Fri, 21 Jul 2023 10:02:21.047537000 JST +09:00],
 [6497, "DN-06497-1", Tue, 16 Jan 2024 14:01:13.255267000 JST +09:00], *deleted
 [6498, "DN-06498-1", Tue, 16 Jan 2024 14:11:19.468340000 JST +09:00], *deleted
 [6499, "DN-06499-1", Tue, 16 Jan 2024 14:17:25.960074000 JST +09:00], *deleted
 [5072, "DN-05072-1", Thu, 05 Oct 2023 10:36:14.349767000 JST +09:00],
 [6915, "DNI-06915", Wed, 21 Feb 2024 09:15:30.039312000 JST +09:00],
 [4818, "DN-04818-1", Tue, 26 Sep 2023 11:03:28.598042000 JST +09:00],
 [6948, "DN-06948-1", Thu, 22 Feb 2024 08:30:45.245433000 JST +09:00],
 [6816, "DN-06816-1", Tue, 06 Feb 2024 11:22:02.753058000 JST +09:00],
 [6815, "DN-06815-1", Tue, 06 Feb 2024 11:15:32.579358000 JST +09:00],
 [6265, "DN-06265-1", Thu, 28 Dec 2023 15:28:06.719106000 JST +09:00],
 [7263, "DNI-07263", Wed, 13 Mar 2024 10:26:16.419751000 JST +09:00],
 [7204, "DNI-07204", Thu, 07 Mar 2024 13:08:04.851946000 JST +09:00],
 [6643, "DNI-06643", Wed, 31 Jan 2024 08:38:25.292832000 JST +09:00]]
  
Be Easy
- [X] [[10546, "DN-10546-1", Wed, 27 Dec 2023 00:21:06.903986000 JST +09:00]]

- [ ] handle remove shipping from an order on shopify
  Shopify shipping can now be deleted, how does that affect orders in brewkeeper?

- [ ] Be-Easy mock up new scenario

- [ ] be easy bug, stocking kegs
https://my.papertrailapp.com/systems/prod-mt1_/events?q=b1f01c16-7b65-47b0-bd9f-cca012897ad3
method=POST path=/barcodes/import format=html controller=BarcodesController action=import

# Create an instance of your controller
Apartment::Tenant.switch!('be-easy')

controller = BarcodesController.new
params={"authenticity_token"=>"BUVECBNEGXhxUzgLmxXJjFGVMGvKF37dw0OJGnV1em7OjlEl4Xa9fJkI6OV_6SSaze6eo04SreOz265crtP1dg", "location"=>{"id"=>"2"}, "commit"=>"在庫計上", "check_all"=>"check_all", "kegs"=>[{"barcode_id"=>"1607", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"85", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"102", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"109", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"115", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"196", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"255", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"275", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"291", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"305", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"318", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"355", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"358", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"386", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"388", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"396", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"404", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"487", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"536", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"590", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"597", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"651", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"669", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"676", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"683", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"692", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"716", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"723", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"725", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"764", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"769", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"775", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"12", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"38", "checked"=>"1", "volume"=>""}, {"barcode_id"=>"1949", "checked"=>"1", "volume"=>""}], "controller"=>"barcodes", "action"=>"import"} 

# Set up a request environment (optional, but may be needed for some actions)
request = ActionDispatch::Request.new({
  'REQUEST_METHOD' => 'POST',
  'PATH_INFO' => '/barcodes/import',
  'PARAMS' => params,
})

controller.request = request
controller.params = params
response = controller.import

# Inspect the response
puts response.body


