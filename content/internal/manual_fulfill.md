---
title: "Manually fulfill an order with allocations"
TableOfContents: true
draft: false
---

```ruby
# get the keg
barcode_text = "KBC S200078"
barcode = Barcode.where(text: barcode_text).one

# find the event type (manually check and confirm)
pp EventType.current.map { |et| [et.id, et.description] }.sort
fulfilling = EventType.find(10)

# build the event
fulfilling_event = Event.new(event_type: fulfilling, barcode_events: [BarcodeEvent.new(barcode: barcode, quantity: nil)])
user = User.where(name: "shaun").one

# execute and force
event = BarcodeService.execute_event(user, fulfilling_event, force: true)
```
