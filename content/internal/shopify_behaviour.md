---
title: Shopify behaviour
TableOfContents: true
draft: false
---

# Line Item Quantities

Unfulfilled line item 
- quantity x4
- fulfillable quantity x4

Restock x1

- quantity x4
- fulfillable quantity x3 (qty - refund qty)
- refunds
  - refund line items 1
    - quantity x1
    - restock type 'cancel'

Edit -> Adjust Quantity (5)

- quantity x6
- fulfillable quantity x5
- refunds
  - refund line items 1
    - quantity x1
    - restock type 'cancel'

Fulfill x3

- quantity x6
- fulfillable quantity x2
- fulfillments
  - line items
    - fulfillable quantity x2
    - quantity x3
- refunds
  - refund line items 1
    - quantity x1
    - restock type 'cancel'

Restock x1 fulfilled item (no restock)

- quantity x6
- fulfillable quantity x2
- fulfillments
  - line items
    - fulfillable quantity x2
    - quantity x3
- refunds
  - refund line items 1
    - quantity x1
    - restock type 'cancel'
  - refund line items 2
    - quantity x1
    - restock type 'no restock'
    - fulfillment status 'partial'  
      *note - there is no info here telling us it was the fulfilled line item that was restocked*

