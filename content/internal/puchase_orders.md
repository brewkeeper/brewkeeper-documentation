---
title: "Purchase Orders & Receiving Vouchers"
TableOfContents: true
draft: false
lastmod: 2024-04-17

description: "Gathering requirements for Purchase Orders"
---

# Questions

## Purchase Order related

### Is Vendor Contact required day 1? NO

<iframe src="https://link.excalidraw.com/readonly/zW9p4mFF8oGEYh6EYDyU" width="500px" height="200px" style="border: none;"></iframe>

Implications: We would just need to figure out exactly what a Vendor Contact is in our system (not a huge deal). Xero have the same idea, so we'd need to sync the idea with them.  
Additional thoughts

- A Vendor Contact is not a customer (in our system), or another billing account
- If a contact is just a name, we could store the names just on the billing account itself
  - eg. "John Smith, Henry, Asher"
- Otherwise, if we also need to store an email address or other info, we would create a small database table for it

## Receiving Voucher related

### What should we call Receiving Vouchers?

- Vendor Delivery Notes (VDN)
- Shipping Note

### Receiving quantity related

#### Can you receive more than the Purchase Order quantity?

LOOK INTO a VDN 'straddling' two POs. (below)

If the constraint is that the receiving quantity MUST be equal or less than the PO quantity, things to consider:

With constraint (as per B30):

- If you want to receive a greater number than is in the PO, it seems the workflow is:
  - Void all RVs for the PO
  - ... (todo)

Without constraint:

- If we find later that this constraint is necessary, we can assume we will have multiple "broken" POs. Reconciling them could take time.

{{ note }}
SUGGESTION:

- PO(1) of 20kg
- VDN of 30kg
- user creates a PO(2) for the "plus alpha" portion
  - PO(1) and PO(2) both 'point' to the VDN

Reason: this is a lot more forgiving, and shows that this is a normal thing that can happen as opposed to it be an erroneous workflow, having to cancel vdns and recreate POs

{{ /note }}

#### Can you Edit Quantities on a Receiving Voucher? YES. this is a good workflow

When you receive a quantity on an RV, inventory is added. I guess this is in case the user enters an incorrect amount by accident and it is less than the quantity they meant to input.  
Instead of making the user VOID the RV, they can just update the quantity.

From the B30 docs

> This will open the Receiving Modal where Dates, Lot Numbers, Quantities, and Prices can all be edited. These edits will also be reflected on the individual inventory records.
> If depletions already exist against the received inventory, users will not be able to edit the quantity to be lower than those associated depletions.  
> _Ex. 20 lbs of 2-Row have been depleted from the 100 lbs received on the order. Users cannot edit the quantity to be lower than the 20 lbs that has already been used._

I believe this means:

- PO for 30kg Grain
- RV for 10kg Grain
- Edit RV
  - Cannot be less than 10kg

### Should we let the user select 'Tax Type' on the RV? Won't it always be the same as the PO? NO - see note

{{< note >}}

Orders can have multiple tax rates per item. Therefore doesnt make sense to ahve an 'overall' one. This is probably why B30 has a 'manual' option.

{{< /note >}}

<iframe src="https://link.excalidraw.com/readonly/HMrhAcnlAGfGgYMuWCZO" width="800px" height="300px" style="border: none;"></iframe>

See "Receiving A Purchase Order" on
Documentation on B30 shows that you cannot select Tax Type, and the video tutorials show you can. I believe there was an update that either allowed it or removed it. It's difficult to know without version numbers

Additional note on Shipping and tax for RVs

tax should always include shipping tax, whether on the item or the whole dn

Shipping and Tax note:
The tax always includes shipping tax, whether on the total or each item.
Eg.

- 2 row item, $10 x1 = .10c tax
- shipping, $10 = .10c tax
  Item tax = .20c

### Do we need "Cancel Vouchers"? (we think no)

_NOTE - if we need these in the future, it would be quite easy to make them after the fact. We can also dynamically get this data without making documents_

These allow you to keep track of what was ordered but did not end up receiving  
What would be the point of browsing and then possibly printing these POs?

**Scenario 1: Did not receive all**

- PO for 30kg Grain
- RV for 20kg Grain
- PO -> "Cancel remaining amount"
  - Creates CV for 10kg Grain

Could we track this without creating a Cancel Voucher?  
Assuming RV line items link to PO line items, we can easily see if the quantities add up or not. One possible solution:

- Scenario 1 occurs (no CV created)
- User marks the PO as "Complete"
  - "Warning! You have not received 10kg Grain. Complete anyway?"
- User can easily filter "Unmet POs"
  - POs that are Complete that have line items where the quantity is less than the related RV items

## When finalizing an RV, do we want both options 'with' and 'without' invoice? (same as b30)

Or are we good for the moment to just have 'Receive', which adds inventory, and then

## General

### Should we allow inclusive & exclusive & tax exempt? YES

yes.

if you order from overseas. customs gets put on things. that is what the tax is. so it doesnt follow 10%. In this case, you need to put a new line item on the order to say 'this is the customs fee', which replaces the tax, which is not exempt on that item.

### Should we allow archive of purchase orders and receiving vouchers? YES

B30 just has everything in 'Complete'. If Lyal or someone is still working on the list, wouldnt he want to somehow say 'this workflow is now done, hide it'

### Should we allow discounts on Purchase Orders and Vendor Delivery Notes? NO AND YES

doesnt make sense to have it on purchase orders

VDNs yes - because later you might want to know why something was cheaper as opposed to assuming they just raised the price on you. eg. might have done free shipping for a month

IF SO, what happens on a vendor delivery note if there is additional quantity and the discount is not the same? Or just differences in general?  
Additional info - B30 does not do discounts on POs or RVs, but Xero does

# Dev Questions

## Purchase Order Details - brewery_billing_email or brewery_shipping_address_email?

The following are proposed address fields examples.  
Note `brewery_billing_email` and `brewery_shipping_email`. I've made it this way to mirror the `DocumentDetail` sender fields.
I think we made it this way because 'email' is not a part of our address table, its always on the order, customer, or billing account table.

```
brewery_billing_address_zip: string
brewery_billing_address_address1: string
... (same as document detail sender and recipient address fields)
brewery_billing_email: string

brewery_shipping_address_zip: string
brewery_shipping_address_address1: string
...
brewery_shipping_email: string
```

## Purchase Order Line Item Detail - include tax information?

TODO: mirror sales orders

- how do we set tax when making an order for shopify / bk

We've found that sometimes external system taxes do not make sense. When you make a PO or RV, you are getting the prices from a vendor who sets all the prices.  
Because of this, maybe we should consider not always calculating tax each time - but set it upon 'save', and then in the future if needed make it a field that the user can modify.

```
variant_id: fk
quantity: int
estimated_arrival: date
unit_price: decimal
total_price: decimal

-- do we include the following fields? --
taxable: boolean (document item details)
taxes_included: boolean (document item details, but its possible this will never be true for POs and RVs)
tax_rate: decimal (document item details)
tax_amount: decimal  (called total_tax on order line items)
```

## Billing Account "Contact Persons"

This depends on if we implement contact persons or not.  
When creating a Purchase Order, you select a Vendor, and optionally a Contact for that Vendor.

Xero has the idea of Contact Persons, it is a part of their Accounting Contact object. As shown below, its just an array.

Note - A contact person only needs an email address if you wish to have multiple contact persons.

```json
{
  "Contacts": [
    {
      "Name": "24 locks",
      "FirstName": "Ben",
      "LastName": "Bowden",
      "EmailAddress": "ben.bowden@24locks.com",
      "ContactPersons": [
        {
          "FirstName": "John",
          "LastName": "Smith",
          "EmailAddress": "john.smith@24locks.com",
          "IncludeInEmails": "true"
        },
        {
          "FirstName": "Henry"
        },
        {
          "FirstName": "Asher"
        }
      ]
    }
  ]
}
```

If we do implement this and we're ok with just first and last name, we could do a day 1 very simple implementation.

```
AccountingContact
id: 1,
name: "24 locks",
contact_persons: "John Smith,Henry,Asher"
```
