---
title: JAN Codes
TableOfContents: true
draft: false
---

# Questions
## is it possible to scan the same JAN code multiple times on the same scan event?
If you scan the same JAN twice, it adds to the quantity of the barcode you just scanned

<!--
## Is it possible for a NUB for a lot not to exist? because this code depends on the NUB existing
TODO: test this properly
i think pressing barcdoes -> new creates a barcode and not a lot inventroy, which is not great

# main test that the problem is fixed
- stock younger lot
  
- allocate order1 with JAN (for younger lot)
  1111
- stock older lot (have to make sure to do this AFTER the above allocation)

- allocate order2 with NUB (for an older lot)
- fulfill order1 (old behaviour: takes the older lot, new behaviour: takes the younger lot)
- fulfill order2 (old behaviour: failed, new behaviour: succeeds)

# test that reverts are correctly handled
- stock lot1
- allocate order with JAN (lot1)
- revert allocation
- revert stock
- stock lot2
- allocate order with JAN (lot2)
- fulfill (this should only try to fulfill inventory from lot2)


# Steps
1. package some BIGI-C301s with 2x different packaging dates
1. create a JAN for the BIGI-C301
  - 1111
1. create 2x orders with BIGI-C301s
  - #2023-Dev-2328
  - #2023-Dev-2329

download the db and test locally

-->
