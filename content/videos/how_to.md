---
title: "How To"
draft: false
weight: 1
tableofcontents: true
---

# Kegs

## Add a Keg Note

<iframe width="900" height="506" src="https://www.youtube.com/embed/yu18ityHhZA?si=M0xN3vRYy4HFXFTs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

# Products

## Archive a Variant Type

<iframe width="900" height="506" src="https://www.youtube.com/embed/MrxVsHx1pf0?si=M64gi9pbefTDZ6ci" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

# Shopify

## Create a Shopify Product

<iframe width="900" height="506" src="https://www.youtube.com/embed/VSf3pSVrGL8?si=kPzFV6JK4AIiGE9v" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

## Track Quantity for a Shopify Variant

<iframe width="900" height="506" src="https://www.youtube.com/embed/3fmY169R2Is?si=KkyyE4g4G3o5TTBN" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

## Activate a Shopify Variant at a Location

BrewKeeper automatically activates inventory at all locations when you create a Variant.  
However, if you create a new Location in Shopify, none of your current variants will be activated in the new location.  

<iframe width="900" height="506" src="https://www.youtube.com/embed/yFWuXZV7sMk?si=92dsfGYj44jkZ_zH" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

# Translations

<iframe width="900" height="506" src="https://www.youtube.com/embed/IOQBsJ_HKkg?si=LgAs1tHlZCuFPHHJ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

# Sales Contracts

## Copy Contracts to Clipboard

<iframe width="900" height="506" src="https://www.youtube.com/embed/rJOz87V9uZU?si=fg0mVExX9erDWVrE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>