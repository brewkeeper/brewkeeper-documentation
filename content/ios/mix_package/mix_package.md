---
title: Mix Package
weight: 1
TableOfContents: true
---

**Purpose:** Create a mixed 6-pack

**Prerequisites**

> 1. User has created Repackaging Instruction in BrewKeeper
> 2. The barcode for the 6 pack you're creating exists
> 2. Sufficient physical inventory (bottles) exist in the locations you will create from

# Step 1: Locate the Mix Package wizard
{{< figure src="home_mp.jpg" title="Home Screen" width="500px">}}

# Step 2: Scan the output variant
The required input variants will appear
{{< figure src="mix_package_2.jpg" title="Mix Package Screen" width="500px">}}

# Step 3: Scan the input variants
{{< figure src="mix_package_3.jpg" title="Mix Package Screen" width="500px">}}

# Step 4: Select the location, adjust the quantity
{{< figure src="mix_package_4.jpg" title="Mix Package Screen" width="500px">}}

# Step 5: Press 'Save'
This will move inventory
{{< figure src="mix_package_5.jpg" title="Mix Package Screen" width="500px">}}

# Inventory Movement

|SKU|<center>Inventory Before|<center>Save|<center>Inventory After|
|---|:-:|:-:|:-:|
| BISS-B301   | 1 | <i class="fas fa-arrow-right" aria-hidden="true"></i> | 0 |
| BIGI-B301   | 1 | <i class="fas fa-arrow-right" aria-hidden="true"></i> | 0 |
| BKSG-B301   | 1 | <i class="fas fa-arrow-right" aria-hidden="true"></i> | 0 |
| BMIX04-B306 | 0 | <i class="fas fa-arrow-right" aria-hidden="true"></i> | 1 |

