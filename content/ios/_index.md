---
title: iOS
weight: 98
---

# Setting up OPN-3102i on an iPad

1. Download OPNTerm application
1. Turn on iPad bluetooth
1. Long hold the button on the OPTICON scanner
1. Connect iPad -> scanner in Bluetooth menu
1. Scan MFi barcode in pdf below on page 1
1. Open OPNTerm application, connect scanner
1. Open the BrewKeeper iOS app
1. Scan a barcode

<iframe src="opn3102i.pdf#toolbar=0" width="100%" height="500px"></iframe>

## Troubleshooting
- Ensure the scanner is connected to the iPad via bluetooth
- Close / reopen BrewKeeper app
