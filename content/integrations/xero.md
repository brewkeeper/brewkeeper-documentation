---
title: "Xero"
weight: 1
tableofcontents: true
draft: false
---

> **Xero Module** must be enabled for any Xero related features. Please contact us for help enabling this module.

# Xero Integration Features

Since v6.7.33, BrewKeeper has vastly improved it's integration with Xero.

*Note - What Xero calls a **Contact**, BrewKeeper calls a **Billing Account**. This document always uses the term **Billing Account**.*

- **Auto Sync with Xero.** All Invoice and Billing Account updates in Xero now automatically go to Brewkeeper. If you create or update a Billing Account in Xero, you will see those changes reflected in the corresponding Billing Account in BrewKeeper.
- **Xero History**. Any communication with Xero is now traceable in our new **Xero History** page, which can be found **Accounting > Xero History**.
- **Create Xero Contacts from BrewKeeper**. You can create Xero Billing Accounts from BrewKeeper at **Accounting > Billing Accounts > New**  
  *You can also* create Xero Billing Accounts **from a Customer** in BrewKeeper at **Sales & Distribution > Customers > Customer > Billing Accounts (box) > Create**. This allows you to auto-fill out the Billing Account Details from the Customer details, without having to type it all in.
- **Improved Invoice creation from Order**. Creating an invoice from an order creates a Billing Account in Xero if one doesnt already exist[^xero-name-generation].
- **Delivery Notes** can be sent to Xero. A Delivery Note can consist of 1 or more orders being shipped to a Customer on the same day. You can create Xero Invoices from Delivery Notes at **Sales & Distribution > Delivery Notes > Send to Xero (tab)**

# Initial Setup

## Connect to multiple Organisations

If your brewery has multiple Xero Accounts, eg.,
- Brewery Organisation PTY LTD
- Brewery Tap Room

You are able to connect to both of these, allowing you to send orders and documents to Xero to become invoices, and to receive webhooks from both

The only caveat is - **both accounts must be connected via the same user with the same email address**. To be more specific

Brewery Accountant "Bob", with email address bob@brewery.com, must use that one email address to connect both Xero Organisations to BrewKeeper.

Step 1: Connect the first account
- Navigate to **Accounting > Xero Settings**
- Click `+ New` -> **Connect to Xero**
- Follow the screen prompts to connect to your Xero Organisation

Step 2: Connect the second account
- Navigate to **Accounting > Xero Settings**
- Click `+ New` -> **Connect to Xero**
- Follow the screen prompts to connect to your Xero Organisation


## Nominate user for scheduled jobs

This user account will be used for the following:
- each night at midnight, BrewKeeper will refresh this user's access token, keeping their connection alive indefinitely
- when updates come from Xero for contacts or invoices, BrewKeeper retrieves these resources from Xero using the nominated user's credentials (see [Turn on updates from Xero (webhooks)](#turn-on-updates-from-xero-webhooks) to enable this feature)

**Accounting > Accounting Settings > Xero Settings > Nominate user for scheduled jobs**

*Note - if you have multiple Xero organisations, the nominated user must be authorised with all organisations*

## Turn on updates from Xero (webhooks)

Any time a Contact or Invoice is created in Xero, it can send those resources to be created or updated in BrewKeeper.

**Accounting > Accounting Settings > Xero Settings > Receive Xero Resources**


## Set contact name priority

When creating Invoices in Xero, if a customer doesnt have a billing account, we need to create one. This setting determine the name on the billing account, using fields from the customer "company", "first name", "last name", "email".

If a field is not available, it falls back to the next field in the prioirty list.

**Accounting > Accounting Settings > Xero Settings > Contact name prioirty**

There are 4 settings you can choose from:

- Business (JP)
- Business (EN)
- Retail (JP)
- Retail (EN)

The only difference between JP and EN, is that JP concatonates the order which first name and last name appears. Below is an example of all four settings.

Example
- First name: John
- Last name: Doe
- Company: Great Beers
- Email: john@greatbeers.com
- Full name (JP): Doe John
- Full name (EN): John Doe

Generated Billing Account Name
- Business (JP) = Great Beers (fallback on full name, then email)
- Business (EN) = Great Beers
- Retail (JP) = Doe John (fallback on company, then email)
- Retail (EN) = John Doe

Q&A
- Q: What if there's only a first or last name?
  - A: It will use the first or last name as the full name. 
  - eg. First name: John, last name: `blank`. Retail (EN) = John

## Set a default billing account

Default billing accounts can be used when creating invoices for selected shops.

**Accounting > Accounting Settings > Xero Settings > Link Organisations to Shops > Gear icon**

In the popup window that appears, you will see `Default Billing Account` option, which allows you to select a billing account



# Sending to Xero

## Documents
### Manually

**Accounting > Documents**

In the filters section, select `[X] Invoiceable` and click `Search`

BrewKeeper will now create what are called **Xero 'Create Invoice' Events** for each document. These events contain all the information that will be sent to Xero. These are for two main purposes:
1. A history of everything that has been sent to Xero
2. If any of these events fail, they can be retried

After the Invoices are created, BrewKeeper will then check to see if any of the orders linked to these documents are **paid** or **partially refunded**, if they are, BrewKeeper will then create **Xero 'Create Payment' Events**

### Creating Payments

A document (delivery note, invoice, delivery note / invoice) can consist of multiple orders. All orders in a document must be fully paid to create Xero Payments for them.

Each order belongs to a shop, and each shop has its own Xero Organisation settings. BrewKeeper will look at those settings in order to know how to deal with the payment of each order.

*Q: If a document can have many orders, and each order can have its own shop, doesnt this mean you could have multiple organisation settings?*  
*A: No, a document can only have orders from the same shop*

**Bank Account**

BrewKeeper retrieves the approprate "bank account" from the organisation settings. This could look something like
- Code: 1001, name: "Credit Card Payment", account_type: "bank_account", class_type: "asset"

BK then looks at the payment method on the order

## Invoicing Orders

**Sales & Distribution > Orders > Send to Xero**

Things to note:

- In BrewKeeper, you can link a Customer to a Billing Account. If the order's customer does not have a Billing Account upon invoice creation, one will be created in Xero[^xero-name-generation], and it will be automatically linked to the Customer.  
  This new contact will only be given the following customer details:
  - Name (from company)
  - First Name
  - Last Name
  - Email  
  Note that the Billing Account's address is not automatically created in this scenario.

[^xero-name-generation]: The settings for how the name should be generated can be found **Accounting > Xero Settings > Contact name priority**. If that name already exists, it will be appended with a number `(n)`. 

# Credit Notes

## Ad-hoc credit notes

What are the actual scenarios?

Mami wants to create an ad-hoc credit note with just a dollar amount \
Do they want to view all credit for a customer? \
How do they want to apply the credit to a customer? Do they want it applied to an order, or to a document? \

## Webhook limitations

**Xero does not send Credit Notes to BrewKeeper** when they are created or updated, *UNLESS they are allocated to an invoice*, in which case the Credit Note comes with the attached invoice.

What this means for our Credit Notes integration, is we cannot trust that Xero and BrewKeeper are truly synchronised until Xero updates their webhooks to include Credit Notes.

If they do update their webhooks to include Credit Notes, this means we can update our Xero Integration so you can:
- Create Credit for an Account without needing to build the credit note from a Delivery Note
- Know how much Credit an account has

https://xero.uservoice.com/forums/5528-accounting-api/suggestions/33582580-please-add-credit-notes-to-the-webhooks-system

## Allocating / Unallocating

When you create a Credit Note in BrewKeeper, it must be built from a Delivery Note. \
When the Delivery Note is sent to Xero, it becomes a Xero Invoice. \
When the Credit Note is sent to Xero, it is allocated to that Xero Invoice. \
You cannot currently unallocate that Credit Note from the Xero Invoice in BrewKeeper.

After implementing the Credit Notes feature, Xero then updated their API to allow **unallocating a Credit Note** from Invoices. We haven't yet implemented this feature but when we do, it would include:
- User opens the Credit Note in BrewKeeper
- User can see which Xero Invoices it is allocated to (which are also Delivery Notes)
- User clicks 'Unallocate' next to one of them
- BrewKeeper sends this request to Xero, and the Credit Note is no longer allocated
- FROM THIS POINT, due to webhook limitations
  - Any updates to that Credit Note in Xero will not be sent to BrewKeeper via webhooks, which means Xero and BrewKeeper can now be out of sync (dangerous)
  - So how do we reliably allocate this Credit Note to another Xero Invoice, knowing our data could be outdated?



