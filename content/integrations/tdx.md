---
title: "TDX"
weight: 1
tableofcontents: true
draft: false
---

TDX is a Japanese accounting software. BrewKeeper exports delivery notes to TDX a compatible CSV.

# Initial Set up

## TDX integration must be be enabled by the BrewKeeper team

Request the module enabled

## Set your sender country

This will set the sender country on each document, which BrewKeeper uses to know if this order is Domestic or an Export (Column G). If the sender country is the same as the receiver country, this is a domestic delivery.

**Sales & Distribution > Settings > Delivery Note & Invoice > Invoices > Delivery Note and Invoice details**

“Country” select box

## Assign billing accounts to customers

Column **E 取引先コード** includes billing account ids.

BrewKeeper will now allow you to download a csv if there are any customers not linked to a Billing Account, as this is a required field in the TDX software.

## Assign a Default Billing Account to your B2C shop

B2C customers don’t need their own billing accounts. The normal workflow is to let them share the same billing account.

1. Create a billing account
**Accounting > Billing Accounts > + New**
2. Create an “Organisation to Shop” link
**Accounting > Accounting Settings > Shop Link Settings**
Press `+` and select the Shop to link to an Organisation and press Create
3. Press the edit icon and select the **Default Billing Account**

## Invoice Default Due Date

If the document does not already have a due date, we retrieve it from the following settings

**Sales & Distribution > Settings > Delivery Note & Invoice > Invoices > Default Due Date**

# How to download a TDX csv

1. Open the Delivery Notes index
**Sales & Distribution > Delivery Notes**
2. Click on the “**Authorised**” tab
3. **Filter the Authorised Delivery Notes** to have Since and Until dates and press “Search”
4. The **TDX CSV download button** will appear

# CSV Columns

## (A) 伝票区切 (Order break indicator)

“1” indicates the first line of the order

## (B) 掛/現金 (Cash vs Credit payment)

Options:

- “11” (Cash)
- “12” (Bank transfer)

掛 = cash receivable (so it has to be reconned later)

現金 = “hard cash” but it truly means customer paid immediately

Currently, we are hard coding this to always be “12”

## (C) 売上日 (Sales Date)

`yyyymmdd`

BrewKeeper will use the document shipping date, which comes from the shipment’s shipping date of the delivery note

## (D) 伝票番号 (Document ID)

BrewKeeper uses the Delivery Note ID for this field

Constraints: Within 6 characters

Note - this will be an issue when they reach 1 million documents

## (E) 取引先コード (Customer ID)

We will use the Billing Account ID here, because there needs to be a way of linking all different customers

You can set up a default billing account for each shop. BrewKeeper will first check if there is a billing account linked to this document, either directly or through a customer. If there is no billing account, it will then check to see if there is a default billing account set up for the shop (see [Assign a Default Billing Account to your B2C shop](https://www.notion.so/TDX-Integration-df5ba26996714cb0a8618dfdab8de9e8?pvs=21).)

If there is not, the user will be informed of the customers that need to be linked before a CSV can be downloaded.

Constraints: 1 - 999,998

## (F) 消費税転嫁方式 (Consumption Tax treatment)

Options:

- 1 (at the time of external tax/delivery)
    - Tax exclusive / time of delivery note
- 2 (taxation)
    - Tax inclusive
- 3 (at the time of foreign tax/billing)
    - Tax exclusive / time of invoice

BrewKeeper will only use options 1 or 2

## (G) 国内/輸出 (Domestic / Export)

Options:

- 1 = Domestic
- 2 = Export

**Sender Country**

BrewKeeper first checks the document sender address country, if that is not set, it will check the Delivery Note & Invoice settings for the sender address country

If neither of these are present, Domestic (1) will be the default

**Recipient Country**

If the recipient country is not set, Domestic (1) will be selected as default

If sender country and recipient country are both set and they differ, it will be set to ”2”

## (H) 先方担当者名 (Customer name)

If there is a default billing account set up, BrewKeeper will use that, otherwise it will use the billing account name associated with the document

Constraints: Within 12 characters

## (I) 請求日 (Billing date)

If the customer does not prefer Consolidated invoices, just use the Issue Date on the document.

If this customer prefers Consolidated Invoices, the billing date would be when the consolidated invoice is supposed to be created for that customer

So if the setting for that customer was that the monthly cut-off was the 20th of every month, the billing date would be for the following 20th from the date the order is being dispatched

eg. 1

| 23rd | 24th | 25th | 26th |
| --- | --- | --- | --- |
|  | TODAY | CUTOFF |  |
- Billing date will be 25th this month

eg. 2

| 23rd | 24th | 25th | 26th |
| --- | --- | --- | --- |
|  |  | CUTOFF & TODAY |  |
- Billing date will be 25th this month

eg. 3

| 23rd | 24th | 25th | 26th |
| --- | --- | --- | --- |
|  |  | CUTOFF | TODAY |
|  |  |  |  |
- Billing date will be 25th next month

## (J) 回収予定日 (Planned collection date)

**Sales & Distribution > Settings > Invoice > Default Due Date**

- 14 days from date of issue
- 30 days from date of issue
- 45 days from date of issue
- 60 days from date of issue
- End of Month
- End of Next Month

If the due date is not on the document, BrewKeeper will check what the settings are for the Default Due Date, and calculate the due date from the issue date, depending on what they’ve set here

## (K) 商品コード (Product code)

Variant SKU. We retrieve this from the linked order line item SKU.

If the user has created a custom document item (edited the delivery note, and added an item), there will be no variant or line item linked, therefore no SKU. In this case, the sku “CUSTOM” will be given

For shipping, BrewKeeper will use “shipping” as the product code

Constraints: Within 16 characters

## (L) 商品名 (Product name)

Document item title - generally the Product + Variant title

eg. Billabong Lager - Can - 360ml 6 Pack

Constraints: Within 40 characters

## (M) 数量 (Quantity)

Line item quantity

## (N) 金額 (Price)

Unit price

### Discounts

| Product | Unit Price | Total Discount | Quantity | Total |
| --- | --- | --- | --- | --- |
| (1) IPA 6 pack | 3600 | 900 | 3 | 8100 |
| (2) NEIPA 6 pack | 3900 | 972 | 2 | 5856 |

BrewKeeper stores line item discounts as total discount, not per unit.

In the above, the total is calculated:

- Unit discount = Discount / Quantity
- Unit price = unit price - unit discount
- (1)
    - **Unit discount** = 900 / 3 = **300**
    - **Unit price** = 3600 - 300 = **3300**
- (2)
    - **Unit discount** = 972 / 2 = **486**
    - **Unit price** = 3900 - 486 = **3414**

## (O) 税率 (Tax rate)

Options

- 1 = taxable
- 2 = untaxable
- 3 = tax exempt

if **(G) 国内/輸出** column is `1`, make this column 1.

if **(G) 国内/輸出** column is `2`, make this column blank (it will ignore this field)
