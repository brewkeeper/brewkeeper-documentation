---
title: Sagawa
weight: 1
TableOfContents: true
draft: true
---

{{< panel status="warning" title="Warning" >}}eHidenII is now deprecated. EhidenIII is the current version{{< /panel >}}

# EhidenII CSV

## Shipping Address Columns
- `お届け先住所1` (Column D Address1)
- `お届け先住所2` (Column E Address2)
- `お届け先住所3` (Column F Address3)

Sample Order shipping address:
- City = "012345"
- Address1 = "abcdefghijklmnopqrstuvwxy"
- Address2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

Note that using the numbers and alphabet in this example will help distinguish how they are joined together below.

Changing shipping address -> Ehiden addresses
1. Join them together - "012345abcdefghijklmnopqrstuvwxyABCDEFGHIJKLMNOPQRSTUVWXYZ"
2. `お届け先住所1` = "012345abcdefghij" (first 16 characters)
3. `お届け先住所2` = "klmnopqrstuvwxyA" (next 16 characters)
4. `お届け先住所3` = "BCDEFGHIJKLMNOPQ" (next 16 characters)

*note that `RSTUVWXYZ` has been cut off*

## Shipping Names
- `お届け先名称1` (Column # Name1)
- `お届け先名称2` (Column # Name2)

Same Name:
- First name = "Shaun William"
- Last name = "Cechner"
- Company = "BrewKeeper"

- `お届け先名称1` = Cechner Shaun Wi
- `お届け先名称2` = BrewKeeper

Changing the name -> Ehiden name fields

`お届け先名称1`
1. Join together Last name and First name with a space
  - "Cechner Shaun William"
2. Keep the first 16 characters
  - "Cechner Shaun Wi"

`お届け先名称2`
1. Strip the first 16 characters of the company field
  - "BrewKeeper"
