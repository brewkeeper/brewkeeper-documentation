---
title: "Shopify"
weight: 1
tableofcontents: true
draft: false
---

# Creating Products and Variants

It is recommended to create your Shopify Products and Variants from BrewKeeper.

If creating your products in Shopify, there are some rules to follow to allow syncing with BrewKeeper

Products:

- A product must have a product type to be able to sync with BrewKeeper. In general for breweries, the product type is Beer.

Product Variants:

- Variants must have skus. Carrierbot returns its rates by looking at SKUS
- The option name for a variant must be "Title" (not Size, Colour, Matrial, Style, etc...)
  - Examples of (Title) option values: "Can - 350ml", "Can - 350ml 6 Pack" \
    Note: these match our "Variant Type" descriptions

{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shopify-product-setup.png" title="Shopify Product example" width="100%">}}

# Link Shops to Sales Channels

If you have a new **Shopify Location**, and you want BrewKeeper to control the inventory levels, you would link that **Shop Location** to a **Sales Channel**

Once BrewKeeper is controlling the inventory levels in Shopify, it is possible BrewKeeper and Shopify inventory levels can be different.

A few ways this can happen:

- In BrewKeeper, user moves stock into sales channel BEFORE it is linked with Shopify (can be common)
- User changes inventory levels in Shopify (this does not change inventory levels in BrewKeeper)
- BrewKeeper somehow fails to update the stock levels (maybe Shopify rejects our request to update, this rarely happens)

The ‘**sync**’ page just shows you if the levels between BrewKeeper and Shopify are different. It is always assumed BrewKeeper has the correct shopify inventory levels

# Shipping Rates - CarrierBot Integration (JP only)

B2B stores are 'tax exclusive' which means tax is added onto the shipping rate
{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shopify-carrier-b2b-settings.png" title="Shopify B2B Settings" width="100%">}}

B2C stores are 'tax inclusive' which means tax needs to be included in the shipping rates
{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shopify-carrier-b2c-settings.png" title="Shopify B2C Settings" width="100%">}}

We create a CarrierBot enpoint for each Shopify store, and for B2C, we multiply the rates by 1.1 (adding 10%).

```yml
# B2B
C306:
  hokkaido: 560
  north_tohoku: 490

# B2C
tax_rate: 1.1 # this setting is a multiplier
C306:
  hokkaido: 560
  north_tohoku: 490
```

# Requested Delivery Dates (JP only)

Some customers do not want their orders delivered as soon as possible.

This app (~$10 p/m) puts a calendar and time picker on your checkout page

https://apps.shopify.com/deliverydate-production

You can customise the calendar in several ways:

- minimum date (eg. 3 days from order creation)
- delivery period (eg. 14 days)
- ...

It also understands timeslot codes from different Japanese shipping companies, including Sagawa, Yamato, Japan Post, etc.

Recommended settings if you wish to include pickup and local delivery on top of your current shipping rates:

- add the date picker, and it is NOT required
- in BrewKeeper, add 'hold times' to sagawa shipping codes which matches the RDD minimum date (eg. minimum date is 3 days, make sure hold time of 3 days)

Further explanation  
if you offer pickup, that generally means the customer can pick it up the next day. This isnt possible if the rdd is a required field, and its minimum is 3 days from the order.  
In this case, you need to let the user not enter any RDD, which just means 'send this as soon as you can'. For pickup, you can add 0 hold days so its shipping date is tomorrow, for sagawa add 3 hold days.

<!--
# Returning (Restock vs Do not restock) line items on an order

## No Restock
BrewKeeper supports 2 workflows when returning an item and unchecking "Restock Item"
(insert img)

1. Order is unfulfilled
  The Reserved inventory is removed.
  `R -> x`
2. Order is fulfilled
  The Reserved inventory has already been removed. No inventory changes.

BrewKeeper does not do anything when the order is *partially* fulfilled.
The reason being, when you click Return on an item, Shopify does not tell us if its the fulfilled item or the non-fulfilled item that is being returned. Without this information its impossible to know whether we should remove from Reserved or not.
Fortunately, it is rare to have a partiailly fulfilled order, and then return an item on that, so this is a corner case.
-->

# Customer addresses limit

Shopify will only share the first 10 addresses of a customer with BrewKeeper (or any integration). A customer will usually do this when they want 1 shopify login for all of their bars.

Shopify does no allow BrewKeeper to access additional customer addresses.

# Multiple shipping codes on an order

Shipping codes can be assigned to Carriers in BrewKeeper, which determines the shipment it will be added to. 

eg.,  
- refrigerated_shipping -> Sagawa
- custom -> Pickup

When editing an order, Shopify allows you to edit the shipping code. 

Doing this adds a second shipping code to the order, which is generally "custom".

The issue here is, if 1 order has 2 shipping codes, each of which is assigned to a different carrier, which one should BrewKeeper choose?

To keep things simple, BrewKeeper just stays with the first shipping code

