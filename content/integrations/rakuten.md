---
title: Rakuten
weight: 1
TableOfContents: true
draft: true
---

**Purpose:** Calculate the Shipping Date and Timeslot from a Rakuten received order

**Prerequisites**

> 1. You have created a 'Shipping Fee' Shipping Code in BrewKeeper (also set up Fulfillment Days, Province Lead Times, Cutoff Times, and Delivery Times)

---

# What is a Rakuten Order?
Shopify can now connect with Rakuten, which is a separate store. When customers place an order on Rakuten, it creates a Shopify Order.

# Characteristics of a Rakuten Order

|Order Attribute|Japanese|English|
|---|:-|:-|
| Source Name  | 楽天市場 | Rakuten Ichiba |
| Shipping Code | Shipping Fee |  |
| Tags   | Rakuten-fulfill | |
| Note Attributes<br>\**Detailed below* | 免責事項<br>支払方法<br>**顧客メモ**<br>為替レート<br>楽天ID | DISCLAIMER<br>Payment Method<br>**Customer notes**<br>FX Rate<br>Rakuten ID |



## Note Attributes
BrewKeeper identifies Rakuten orders by their note attributes, which come on the created order. \
Currently we are only concerned with the **Customer notes** note attribute.

## Example 'Customer notes' Note Attribute
**English**
```
Customer notes
2020-07-09 (thurs)In the morning[age check:]in accordance with the minors drinking prohibition law, please inform us of the age of the orderer.please indicate your age below.( )age
```
**Japanese**
```
顧客メモ
2020-06-08(月)午前中
```

# How BrewKeeper processes a Rakuten Order
*Note: Currently BrewKeeper only supports the English version of Customer notes. A Japanese Customer notes will display a Diagnostics Event error for the user to Manually update the order.*

## Step 1: Extract the information
Example Customer Note: '`2020-07-09 (thurs)In the morning[age check:]...`'


BrewKeeper extracts 2 pieces of information from the Customer Notes.
* Requested Delivery Date - `2020-07-09`
* Timeslot - `In the morning`

### Requested Delivery Date
Is identified by looking for 4 numbers, followed by a dash, 2 numbers, another dash, and another 2 numbers.\
eg. **2020-07-09** (thurs)In the morning[age check:]...
### Timeslot
Timeslot looks for any text from the Requested Delivery Date, up until the first open square bracket: `[`\
eg. 2020-07-09 (thurs)**In the morning**[age check:]...

This will also work if there is no RDD\
eg. **In the morning**[age check:]...

## Step 2: Calculate the Shipping Date from the Requested Delivery Date
Given the Shipping Code from Rakuten (Shipping Fee), BrewKeeper now calculates a Shipping Date based on:
* Shipping Address
* Fulfillment Days, Province Lead Times, Cutoff Times, and Delivery Times

With the extracted Requested Delivery Date, BrewKeeper will attach it's own Note Attribute "Shipping Date" and place the calculated Shipping Date there.

## Step 3: Calculate the Timeslot code from the Timeslot
The Timeslot code is used for Sagawa Shipping Slips

Timeslot options from Rakuten include:
* In the morning
* 12:00-14:00
* 14:00-16:00
* 16:00-18:00
* 18:00-21:00

If you have set up your Delivery Times, and the timeslots match these options, BrewKeeper will update these timeslots
* In the morning <i class="fas fa-arrow-right" aria-hidden="true"></i> 01
* 12:00-14:00 <i class="fas fa-arrow-right" aria-hidden="true"></i> 12
* 14:00-16:00 <i class="fas fa-arrow-right" aria-hidden="true"></i> 14
* 16:00-18:00 <i class="fas fa-arrow-right" aria-hidden="true"></i> 16
* 18:00-21:00 <i class="fas fa-arrow-right" aria-hidden="true"></i> 04

If BrewKeeper is unable to find a matching timeslot, it will just keep the current timeslot
