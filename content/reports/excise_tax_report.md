---
title: "Excise Tax Report (JP)"
weight: 1
tableofcontents: false
draft: false
---

**Purpose:** Generate a Tax Excise Report compliant to Japan standards

**Prerequisites**

> 1. You have created a **Bonded Area** in **Warehouse > Settings**
> 1. You have created in **Accounting > Settings**

**Accounting > Reports > Excise Tax Report**

The first and last days of the previous month are automatically input into the `From` and `To` date fields for convenience when you first visit this page.

The table you are presented includes all physical inventory that has:

1. Left any bonded areas  
   _This may include multiple bonded areas with multiple locations_  
   _This does not include inventory transferred between bonded areas_
1. AND THEN entered into any other pile  
   _This does not include destroyed inventory_
1. Within the timeframe selected
1. Grouped by Product, SKU and variant size

What about **returns**?
If the inventory enters a bonded area, it will not be seen by this report.

What about **reverts**?
A revert is an accidental scan that has been undone, so you need to think about it as never having happened in the first place.

Reverts are calculated and automatically subtracted from the table you see.

Another table is shown below which shows you any returned inventory  
Returned table includes all physical inventory that has:

1. An 'undo' scan associated
1. Entered any bonded area
1. FROM another pile  
   _This does not include destroyed inventory_
1. Within the timeframe selected
1. Grouped by Product, SKU and variant size

# Reverts

A revert must take place during the report time to take affect.  
Eg. If a keg is fulfilled on the last day of the month

# Low fills

Make a new row in the spreadsheet, because there is a different volume.

# Columns

## The following columns are for transfers between bonded areas (?)

### F ① 総移出数量 ML

Total volume left bonded area

### G ② 未納税移出数 ML

The volume left bonded area WITHOUT paying tax (eg. transfer between bonded areas)

### G ③ 輸出免税数 ML

The volume exported (therefore not subject to tax)  
NOT CURRENTLY SUPPORTED

### H ④ 税標準数 (① - ② - ③ ) ML

Taxable volume = ① - ② - ③
