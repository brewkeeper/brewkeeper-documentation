---
title: "Stock Ledger Report"
weight: 1
tableofcontents: true
draft: false
---

# Generating worksheet (tab) names

Worksheet names are generated with the following format:
- `{sku} {product title}`

## Constrants
Any **invalid characters** will be substituted with `-`  
The worksheet name will be truncated to **31 characters**  
If the worksheet name has been used, it will be appended with `(n)` suffix  

## Example
Example worksheet title that breaks all the rules
- sku: `IPAG?-C301`
- product title: `Double Indian Pale Ale of the Gods?`

Worksheet name:
- from: `IPAG?-C301 Double Indian Pale Ale of the Gods?`
- to: `IPAG--C301 Double Indian Pale A`  
  <small>*If this worksheet name is already taken (which should not be possible):*</small>
- to: `IPAG--C301 Double Indian Pal(1)`  
