---
title: JAN Codes
weight: 1
TableOfContents: true
draft: true
---

**Purpose:** Attach a barcode to a variant

**Prerequisites**

> 1. You have created a 'Shipping Fee' Shipping Code in BrewKeeper (also set up Fulfillment Days, Province Lead Times, Cutoff Times, and Delivery Times)
> 2.
> 2.
---

# What is a JAN Code?
