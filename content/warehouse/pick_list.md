---
title: Pick List
weight: 1
TableOfContents: true
---

3 recipe files are all required to use the pick list page
- `brewerya-bundled-recipes.yml`
- `brewerya-packing-recipes.yml`
- `brewerya-unpacked-recipes.yml`

See below for example files

# Pick List
`/orders/pick_list`

## Breakdown
Example of how things are 'broken down' using `brewerya-packing-recipes.yml`[^packing-recipes]

**Breakdown** B524: 1, B512: 1, C312: 1, C306: 1

| Order | Line Items | *Broken down* |
|---|---|---|
|   001 | 1x B306, 1x B512 | 1x B524          |
|   002 | 1x C312, 1x C306 | 1x C312, 1x C306 |
|   003 | 1x B703          | 1x B512          |


## EhidenCSV

| ... | 品名1 (`unpacked-recipes.yml`)[^unpacked-recipes] | ... | 出荷個数 (`bundled-recipes.yml`)[^bundled-recipes] | ... |
|-----|-----------------------------|-----|-----------------------------|-----|
| 001 | 缶（ボトル）ビール）ビールｘ１８ |     |                            1 |     |
| 002 | 缶（ボトル）ビール）ビールｘ１８ |     |                            1 |     |
| 003 | 缶（ボトル）ビール）ビールｘ３   |     |                            1 |     |


# YML Examples

## brewerya-bundled-recipes.yml
```yml
# This is for the total packages included in the Ehiden CSV (also for carrierbot rates, but bk does not currently request those from here)
# This means - anything that can be tied together will be counted as 1 package. eg. C318s
oct_2021:
  start_datetime: 2021-10-01 00:00:00.00 +9
  recipes:
    cans_single_to_six_pack:
      input_fragment: "C301"
      input_quantity: 6
      output_fragment: "C306"
      output_quantity: 1

    bundle_twenty_fours:
      input_fragment: "C324"
      input_quantity: 2
      output_fragment: "C324B"
      output_quantity: 1

    cans_twenty_four_pack_4:
      input_fragment: "C306"
      input_quantity: 4
      output_fragment: "C324"
      output_quantity: 1

    cans_twenty_four_pack_3:
      input_fragment: "C306"
      input_quantity: 3
      output_fragment: "C324"
      output_quantity: 1

    bundle_sixes:
      input_fragment: "C306"
      input_quantity: 2
      output_fragment: "C306B"
      output_quantity: 1
```

## brewerya-packing-recipes.yml
```yml
# This tells logistics which boxes they need to grab to be able to pack. On the orders/pick_list page, it will show up in the 'Breakdown' section. Eg: line item = 6xb301, Breakdown: 1xB306
oct_2021:
  start_datetime: 2021-10-01 00:00:00.00 +9
  recipes:
    cans_single_to_six_pack:
      input_fragment: "C301"
      input_quantity: 6
      output_fragment: "C306"
      output_quantity: 1

    bundle_twenty_fours:
      input_fragment: "C324"
      input_quantity: 2
      output_fragment: "C324B"
      output_quantity: 1

    cans_twenty_four_pack_4:
      input_fragment: "C306"
      input_quantity: 4
      output_fragment: "C324"
      output_quantity: 1

    cans_twenty_four_pack_3:
      input_fragment: "C306"
      input_quantity: 3
      output_fragment: "C324"
      output_quantity: 1

    bundle_sixes:
      input_fragment: "C306"
      input_quantity: 2
      output_fragment: "C306B"
      output_quantity: 1
```
## brewerya-unpacked-recipes.yml
```yml
# What is the smallest each variant type can be broken down to, and how many
oct_2021:
  start_datetime: 2021-10-01 00:00:00.00 +9
  recipes:
    c324b:
      input_fragment: "C324B"
      input_quantity: 1
      output_fragment: &single "缶（ボトル）ビール"
      output_quantity: 48

    c324:
      input_fragment: "C324"
      input_quantity: 1
      output_fragment: *single
      output_quantity: 24

    c312:
      input_fragment: "C312"
      input_quantity: 1
      output_fragment: *single
      output_quantity: 12

    c306b:
      input_fragment: "C306B"
      input_quantity: 1
      output_fragment: *single
      output_quantity: 12

    c306:
      input_fragment: "C306"
      input_quantity: 1
      output_fragment: *single
      output_quantity: 6
```


[^bundled-recipes]: `brewerya-bundled-recipes.yml`: This is for the total packages included in the Ehiden CSV (also for carrierbot rates, but bk does not currently request those from here). This means - anything that can be tied together will be counted as 1 package. eg. C318s
[^packing-recipes]: `brewerya-packing-recipes.yml`: This tells logistics which boxes they need to grab to be able to pack. On the orders/pick_list page, it will show up in the 'Breakdown' section. Eg: line item = 6xb301, Breakdown: 1xB306
[^unpacked-recipes]: `brewerya-unpacked-recipes.yml`: What is the smallest each variant type can be broken down to, and how many
