---
title: Fulfillments
weight: 1
TableOfContents: true
draft: true
---

{{< panel status="warning" title="Warning" >}}Deprecated. Fulfillments are now handled via the Dispatch Centre{{< /panel >}}

# Fulfill in Shopify with scanned allocations
On the iPad you have allocated variants to an order. This creates what we call 'allocations'.

When you go to the **Orders > Fulfill** page, you are shown a list of allocated line items ready to be fulfilled via Shopify.

After you press fulfill, it will
- Create **order shipments** (`shipment_dispatch` sales event) so BK knows which physical inventory to move
- **Fulfill the line items** in Shopify
- **Move the appropriate inventory** (FIFO) in BrewKeeper (`fulfillment_created` sales event)

## Create Order Shipments
For each selected order, it creates a `shipment_dispatch` sales event. This sales event has an order shipment attached.

The **Order Shipment** currently has 
- an ehiden number and a tracking number (soon to be deprecated)
- the allocations for this order that you scanned
- *note* if a line item is a fee, like a COD Fee, a separate allocating scan event will be created here for the fee, and it will be marked as processed.

## Fulfill the line items in Shopify
First we get the correct Shop Location. The Shop Location is not always on the order, as Shopify does not provide one if the order was created for the default Shop Location (strange behaviour).

This means it is retrieved from **Order > Shop > Shop Settings > Shop Location** [^shop_locations]

[^shop_locations]: *Soon to be deprecated, this unfortunately means you can only fulfill via the default shop location for the order's shop. We should be using Shopify's new **FulfillmentOrder** to get the correct Shop Location.* 

It then:
- gets all the line items it will fulfill
- creates the **shopify fulfillment**, attaching the tracking number and tracking company if present
- attaches the **shopify fulfillment** to the **order shipment**.

## Move the appropriate inventory
When BrewKeeper receives a shopify fulfillment, it creates a `fulfillment_created` sales event.

From here it:
- Gets the relevent allocations from the linked **order shipment** that are *not* fees
- Allocation scan event's **location to** = new fulfillment scan event's **location from**
- Create a mock fulfill scan event with the allocations and location information

## Requirements
- A Default Shop Location set up in BrewKeeper for the Shop you are creating Shopify fulfillments for
- A `Ready to Ship` location
- An `Allocate to Order` event type that has a location_to of `Ready to Ship`
- A `Fulfill` event type that has a location_from of `Ready to Ship`

----

# Shopify POS Fulfillments with FIFO
When a POS order comes in, BrewKeeper is able to automatically move physical inventory via First In First Out (FIFO). For this to work, it executes a mock Scan Event and it selects the oldest inventory to move from a location.

How does it know which scan event to use?

BrewKeeper receives a POS fulfillment, then:
- It checks there are no shipments / allocations. If it does have an allocation, it doesnt do anything (this workflow is reserved for orders with no shipments)  
It's important to understand that POS orders have no allocations, by nature they are just cans / bottles sold from a fridge. The staff don't have the time to create an order, allocate to the order, then fulfill the order for each customer.
- It creates the `fulfillment_created` sales event.
- It gets the **shop location** of the fulfillment
- It looks for a **shop location** -> **location** mapping.
- It finds a **fulfilling event type** that has a **location from** of the mapped location.[^fulfilling_event_type]
- It loops through all the line items -> shop variants -> variants, and retrieves the oldest inventory, adding them to the scan event, and executes it.

[^fulfilling_event_type]: Is currently an event type that has a **location from** the specified location, and no **location to**.

## Requirements
- A `Shop Location <-> Sales Channel` **Mapping**  
BreweryA maps the `POS Shop Location` -> `POS Sales Channel`
- A `Location` -> `Sales Channel` **Location Purpose**  
Think of a **Location Purpose** as a single place in your fridge that is intended for one or more sales channels.  
In this case, BreweryA has specified the `POS Sales Channel` is in the `TapRoom` location. 
- A `Fulfilling Event Type` with  a location from of `TapRoom`, and no location to.


