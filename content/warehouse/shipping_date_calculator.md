---
title: Shipping Date Calculator
weight: 1
TableOfContents: true
draft: true
---

**Purpose:** Calculate the Shipping Date for an order

# Settings

## Fulfillment Days*
\* required for shipping date to be returned

Days orders are fulfilled
eg.

|Sun|Mon|Tue|Wed|Thur|Fri|Sat|
|---|:-|:-|:-|:-|:-|:-|
| - | Y | Y | Y | Y | Y | - |


## Cutoff Time
If an order is created **before** cutoff time, it is shipped the same day

> Cutoff Time: **8am**  
Order received: **7:59am**  
Shipping Day: **same day** + **hold days**


If an order is created **after** cutoff time, it is shipped the next possible day

> Cutoff Time: **8am**  
Order received: **8:00am**  
Shipping Day: **next fulfillment day** + **hold days**

## Hold Days
Days to hold before calculating shipping days
eg.

> Fulfillment days are: **Mon -> Fri**  
Hold time: **1 day**  
Cutoff time: **midnight**  
Order comes in **Monday**  
Hold: **Tuesday**  
Shipping date: **Wednesday**  

Hold times take into account fulfillment days and blackout dates

## Blackout Dates
No deliveries on these specific dates

## Delivery Times
Time period your customer can expect their delivery. Optionally, you can include the Sagawa time code to include on your Shipping Slip
eg.

> No preference  
01 08:00 - 12:00  
12 12:00 - 14:00  
14 14:00 - 16:00  
16 16:00 - 18:00  
04 18:00 - 21:00

## Province Lead Time
The number of days it takes to get to a location
eg.

| PostCode | Lead Time (days) |
|---|---|
| JP-01 | 3 |
| JP-02 | 2 |
| JP-03 | 2 |

# Examples

*notes:*
- *order time doesnt matter if there is no cutoff time*
- *the following scenarios assume orders are fulfilled every day*
- *cutoff of midnight means 00:00 not 24:00*

| | Order time | Cutoff time | Lead days | Hold days | **Shipping date** |
|:--: |:---:|:---:|:---:|:---:|:---:|
| *a* | - | - | - | - | **+1 day** |
| *b* | 7:59 | 8:00 | - | - | **same day** |
| *c* | 8:00 | 8:00 | - | - | **same day** |
| *d* | 8:01 | 8:00 | - | - | **+1 day** |
| *e* | - | - | 1 | 0 | **+1 day** |
| *f* | - | - | 0 | 1 | **+2 days** | 
| *g* | - | - | 1 | 1 | **+2 days** | 

Why does *d* and *e* have a different shipping date?  
If the order has no rdd, it defaults to 'today', and the shipping date is calculated backwards from the rdd.
- *e:*
  - rdd = today
  - lead time = +1 day
  - therefore: shipping date = +1 day (lead time)
- *f:*
  - rdd = today, 
  - lead time = 0 days
  - cutoff time = midnight = +1 day
  - hold days = +1 day
  - therefore: shipping date = +2 days (+1 day due to not making it before cutoff, and +1 hold day)

# Questions and Answers

## Why is my Shipping Date is not being updated?

### Update the Shipping Date from BrewKeeper
The shipping date is only calculated when the order is created, or when you update the RDD in BrewKeeper. Updates to the order in Shopify will not update the Shipping Date, unless you update the Shipping Date field directly

## 
