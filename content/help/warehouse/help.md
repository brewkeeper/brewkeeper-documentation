---
title: Warehouse
weight: 1
TableOfContents: true
---

# Failed Sales Event. "cannot remove R:1 from Shopify B2B BCOF-C324 (U:0 R:0 B:0) - 'reserved' cannot go below 0."
## Possibility 1
Sometimes a user might accidently fulfill from Shopify, then cancel the fulfillment, then fulfill via BrewKeeper

This is what happened in this circumstance:
Order: #2021-1234
- 1x BCOF-C324

Order Created
- BCOF-C324 1x U->R

Fulfillment Created (Shopify Fulfill)
- BCOF-C324 1x R->x

Fulfillment Cancelled (bk sees this as Fulfillment Updated)
- NO INVENTORY MOVEMENTS (not supported by BK)

Fulfillment Created (BK Fulfill)
- BCOF-C324 1x R->x

We do not allow Reserved to go negative. It's a good thing there was an error here.  
However, consider if there WAS 1 in Reserved. This order would have taken from reserved twice! And then the order that SHOULD have taken from reserved would not be able to, resulting in the SAME error, only making it more difficult to track.

### Fix Solution

Open the failed Sales event and 'Ignore' it, with a detailed message

```
fulfillment_created event triggered by Shopify (sales event id 9947) which moved inventory, then fulfillment was cancelled by shopify (9956) which did not move inventory (not supported by bk), then was fulfilled by BK, so tried to move inventory again but could not (R can not go below 0)
```

## Possibility 2
If an order is created and the line items have shop variants that are not linked in brewkeeper, it cannot reserve inventory.

This is what happened in this circumstance:

Order: #2021-1234
- 1x BCOF-C324

Order Created
- NO INVENTORY MOVEMENT (not linked)

Variant -> Shop Variant Link

Manual stock movement
- 1x BCOF-C324 x -> U

Order Fulfilled
- 1x BCOF-C324 R -> x (FAIL! nothing in R) 

### Fix

- I manually moved 1x U -> R
- I re-executed the sales event to move R -> x

# I fulfilled from Shopify instead of BrewKeeper orders/fulfill. How do I fix it?

The process of fulfilling orders impacts multiple parts of BrewKeeper:

-   your order management system must be updated to mark the order as having been fulfilled
-   sales inventory must be updated to identify that sales stock no longer needs to be reserved for that order
-   any physical inventory that was allocated to any shipments for that order may be automatically removed

In other words, fulfilling an order from a BK perspective may impact more than just the order management system.

If you rely on BK to automatically update your physical inventory (i.e., you wont be scanning or manually updating the inventory) you should initiate your fulfillments from the BK fulfillment screen. Otherwise you'll find yourself in a situation where your physical inventory does not match available sales stock.



## Understanding the issue
   When you fulfill from BrewKeeper via the *Orders/Fulfill* page, BrewKeeper creates a Sales Event, and executes it.  
   This removes inventory from Reserved (it deletes it).

   To see how your stock moves, you can go to *Sales -> Stock History*.

   *Correct workflow with orders/fulfill*

   | id            | Order      | From  |    | To    | SKU       | Quantity |
   |---------------|------------|-------|----|-------|-----------|----------|
   | 1 (reserved)  | #2021-1100 | B2C:U | -> | B2C:R | AIPA-KL10 |        1 |
   | 2 (fulfilled) | #2021-1100 | B2C:R | -> |       | AIPA-KL10 |        1 |

   The above table shows:
   1. A keg (KL10) first being reserved via an order being created
   2. The keg then being fulfilled via orders/fulfill

   *Incorrect workflow with Shopify fulfill*

   | id           | Order      | From  |    | To    | SKU       | Quantity |
   |--------------|------------|-------|----|-------|-----------|----------|
   | 1 (reserved) | #2021-1100 | B2C:U | -> | B2C:R | AIPA-KL10 |        1 |

   The above table shows:
   1. The order being reserved (as before)
   2. No stock being moved when the Shopify user fulfiled via Shopify.

   *Why isnt stock removed when fulfilled via Shopify?*
   BrewKeeper owns the stock, and it manages the inventory.

## Fixing the issue
   *Solution A (preferred): Create a 'Fulfillment Created' Sales Event and execute it*

   This needs to be done by developers

   *Solution B: Manually remove from Reserved*
   1. *Sales -> Sales Channels*
   2. Go to advanced mode by adding suffix ~?advanced=1~ to the URL in the address bar
      - ~https://mybrewery.brewkeeper.io/inventory/variant_allocations?advanced=1~
   3. *Move Inventory* button
      - Variant: *AIPA-KL10*
      - Quantity: *1*
      - From: *B2C*
      - To: Remove Inventory
      - (select partition section)
      - From: *Reserved*
      - To: Unrestricted
      - Notes: *re-adjusting for variance caused by order #2021-1100 being fulfilled via Shopify*

-----

# I need to unpack a mixed pack

  We don't have a great workflow for unpacking mixed packs just yet - it relies on finding the scan events and 'reverting' them. This is not a great process, but when it because priority it can definitely be improved.


## Understanding the issue

   When you package a mix pack, it needs to create 2 scan events to:
   1. Destroy the input inventory
   2. Create the output inventory

   For a mix pack, there will only ever be 1 output inventory (you are creating a six pack), and 1 or more input inventories (you are creating a six pack from 3x PROD1-CAN and 3x PROD2-CAN)

   So when you repackage, BrewKeeper will create these 2 scan events for you.

## Fixing the issue

   Apologies for this process, it's not great.

   1. Find the barcode you want to undo (you can do this in the *Warehouse -> Inventory* page)
   2. Copy the barcode (in the right most total column, click the total quantity of the row)
   3. Navigate to https://yourbrewkeeperaddress.io/barcodes/COPIED_BARCODE
   4. Find the Scan event you want to =revert=, take note of the *Created at*
   5. Navigate to *Warehouse -> Scan History* and put in the *Created at* date, and find the scan events
      1. The repackaging consume (input)
      2. The repackaging create (output)
   6. For each of the input and output, click on the *Processed at* column, opening the link in a new tab
   7. For each of these pages, open another new tab from the *Scanned Barcodes*
   8. append =&advanced=1= to the end of the URL, and you should now see a =revert= option next to the highlighted row (this row is the event you want to revert)
   9. Click the revert
   10. Repeat from step 7. Until you've reverted all of the barcodes scanned
