---
title: "Purchase Orders and Receiving Vouchers"
TableOfContents: true
draft: true
---

# Purchase Orders

# Receiving Vouchers

## Shipping totals behaviour & tax totals beahviour

Updating the total shipping distributes shipping amounts over the line items

eg.

```
- Hops x20kg
- Grans x10kg

1. User adds total shipping: $15
Shipping is distributed as follows:

- Total Shipping: $15
- Hops x20kg
  - shipping: $10
- Grains x10kg
  - shipping: $5

2. User updates Hops shipping to $5

- Total Shipping: $15 *(note total shipping amount does not change)
- Hops x20kg
  - shipping: $5
- Grains x10kg
  - shipping: $5

3. User attempts to receive
"Total shipping does not match allocated shipping"
```

The idea here is in step 2 the user has updated a line shipping, but has forgotten to update the other line shipping. We dont want anything automatic to happen at this stage, they have made a user error that needs to be addressed.

## Cancelling VDNs

> If inventory depletions tied to the lotted items received exist, Voiding the Receiving Voucher will not be permitted. If the RV still needs to be voided, the user must manually unlink* all associated depletion events from the lotted quantities on the RV first, which will then allow the RV to be voided.  
> *Unlink the subtraction events by changing their lot number to a different lot number or untracked.\*
>
> - B30

Cancelling a VDN will remove of that lot inventory from all warehouses.  
If any of that inventory has been depleted (eg. for brewing), those brews need to be unlinked.  
In other words. The total lot inventory needs to be available in your warehouses to be removed when that VDN is cancelled.
