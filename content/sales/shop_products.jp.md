---
title: "ショップ商品"
weight: 1
TableOfContents: true
draft: false
---
# ショップ商品で何ができますか？

単一の商品は、複数のショップで販売することができます。これを行いたい理由は複数ありますが、以下のような理由が含まれます：
* 異なる顧客に対して異なる価格を提供したい
* ビジネス間（B2B）での販売は、顧客への販売（B2C）と異なる税務上の影響を持つ

BrewKeeperでは、異なる在庫レベルと価格を持つ店舗ごとのショップ商品を作成することができます。

下の図では、製品（IPA 2024）がB2BおよびB2Cショップの両方で販売されており、重要な違いがあります：
* 各ショップで価格が異なります
* B2Bではシングル缶が提供されず、B2Cでは樽が提供されません

<center>{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shop-products-product-links.png" title="ショップ商品リンク" width="600px">}}</center>

BrewKeeperでは、すべてのショップで在庫レベルを簡単に確認し、変更することもできます。

<center>{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shop-products-sales-channels.png" title="販売チャネル" width="600px">}}</center>


# 新しいショップ商品を作成する方法
ショップ商品は商品から作成されます。これにより、ショップ商品とバリアントのタイトル、またはバリアントのSKUの入力ミスの可能性が少なくなります。

<ol start=1><li><b>販売 & 配布 > ショップ商品</b>へ移動します</li></ol>
<ol start=2><li>ショップを選択します</li></ol>

<center>{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shop-products-create-step1&2.png" title="ステップ 1 および 2" width="600px">}}</center>

<ol start=3><li><b>+ 新規</b>をクリックします</li></ol>
<center>{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shop-products-create-step3.png" title="ステップ 3" width="600px">}}</center>

<ol start=4><li>作成したいショップバリアントを選択し、<b>作成</b>をクリックします</li></ol>
<center>{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shop-products-create-step4.png" title="ステップ 4" width="600px">}}</center>

## ビデオデモンストレーション
<iframe width="900" height="450" src="https://www.youtube-nocookie.com/embed/TbAEwdHmoGg?si=Io_bS6TysQtRpML3" title="YouTubeビデオプレイヤー" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>


# 質問と回答
## Shopify関連
### ショップバリアントのSKUを変更するにはどうすればよいですか？
現在、ショップバリアントのすべての更新はShopifyで行う必要があります。

<ol start=1><li>ショップ商品ページでShopifyアイコンをクリックします。これにより、Shopifyでショップ商品が開きます。</li></ol>
<center>{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shop-products-edit-shopify-variant1.png" title="ステップ 1" width="600px">}}</center>

<ol start=2><li>(Shopify内で) バリアントセクションまでスクロールダウンし、更新したいバリアントをクリックします。</li></ol>
<center>{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shop-products-edit-shopify-variant2.png" title="ステップ 2" width="600px">}}</center>

<ol start=3><li>(Shopify内で) ショップバリアントの詳細を更新します。</li></ol>
<center>{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shop-products-edit-shopify-variant3.png" title="ステップ 2" width="600px">}}</center>

