---
title: "Sales & Distribution"
weight: 1
---

<!-- 
# Stocking of Sales Inventory

At any time, you can adjust the levels of your stock in the BrewKeeper Inventory Management module.  
The screenshot below shows a snapshot of your sales inventory. 

[Screenshot]

Product 6-Day Weekend has 1x 10l Keg available for sale in the TapRoom. This is shown by the 1 in the U column, on the BSKM-KL10 row. (U = Unallocated / Available for sale)

Product New Day has 3x 12-Packs reserved for customers on their Shopify store, as shown by the 3 in the Shopify column, on the BCTJ-B312 row. (R = Reserved for an order)

Numbers will appear in the ‘B’ column (Blocked) if you do not have enough inventory to meet a reserved number. For instance, if you were to move a 6-Day Weekend 15L Keg (BSKM-KL15) from Shopify -> B2C, you will no longer have enough kegs to meet the required amount, placing a 1 in the B column (keeping 1 in reserved).

## Moving Inventory (Adding / Subtracting)

The Move Inventory action in the screenshot above allows you to record that you’ve moved inventory into your fridge, by selecting a Variant (tracked by SKU as per the screenshot above), a Quantity to be added to BrewKeeper inventory, and a To partition. The new inventory will be created in the system as Unrestricted U (per the screenshot above).

[Screenshot]

# Allocation of sales inventory to different sales channels

The same module allows you to reallocate sales inventory between different sales channels.

The Move Inventory action discussed above also allows you to reallocate inventory between sales channels (Partitions), by selecting a From Partition and a To Partition. Unrestricted stock will be moved from the selected From Partition to the selected To Partition.

[Screenshot]

# Fulfillment of sales inventory to the TapRoom

Since she would also have access to the BrewKeeper server, Gemma would be able to fulfill stock to the TapRoom, which would remove it from inventory.

Consistent with the actions described above, Gemma would use the Move Inventory action to record that she’s moved inventory from the fridge, by selecting the Variant, the Quantity, and the From Partition (probably TapRoom). The Unrestricted inventory will be removed from the system as requested (TapRoom fulfillments do not utilise the Sales Request / Order process detailed further below, and therefore does not track inventory as Reserved; inventory remains Unrestricted for the entire workflow).


# Creation of Sales Requests for wholesale sales inventory

BrewKeeper has recently added the ability to create ad-hoc Sales Requests from available sales inventory. 

This means that sales consultants can only generate Sales Requests for sales inventory which is currently in stock, and which is not already reserved for Orders. The screenshot below shows the Sales Request index page, which allows you to review existing Sales Requests or create a new one.

[Screenshot]

The screenshot below shows how a Sales Request is created. The consultant selects a Customer (which will be downloaded via the Xero interface). The consultant can select from available Shipping and Billing Addresses if available, and also Delivery and Payment methods. The consultant can then add line items to the Sales Request one at a time, selecting from available (Unrestricted) stock.

[Screenshot]

Once the Sales Request has been created, it remains in state as a “Draft Order” until such time as they are approved, and an Order is created. This part of the workflow is described below. 

[Screenshot]

# Conversion of Sales Requests to Orders

Sales Requests do not Reserve sales inventory. Sales consultants are able to create Sales Requests for Unrestricted inventory, but the approval of the Sales Request to create an Order is a separate process. Once you (Matt) have had a chance to review a Sales Request and approve it (using the Create Order button shown above), BrewKeeper creates an Order with all of the specified details. 

Per the Stocking of Sales Inventory section above, at this point the relevant inventory is converted from Unrestricted to Reserved. This means that the inventory in the Inventory Allocation screen would be updated accordingly, and during the creation of any new Sales Requests the amended Available (Unrestricted) inventory would be displayed.

The Order associated with the Sales Request in the image above, is shown in the screenshot below. 

[Screenshot]

# Fulfillment of sales inventory to satisfy Orders

Once the sales inventory has been dispatched to the Customer, then you (Matt) are able to mark the Order as Fulfilled (using the fulfill button shown above). This removes the Reserved inventory from the system, so that it is no longer visible in the Inventory Management system. 


# Creation of Invoices in Xero

The final step in the workflow (not yet completed by BrewKeeper) is the creation of Invoices in Xero at the time of the Fulfillment. We’ve already conducted a proof of concept of this function, but have not developed an official module to manage the interface to Xero. We anticipate that this will be complete in the next couple of weeks.
 -->
