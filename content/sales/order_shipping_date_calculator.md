---
title: Shipping Date Calculator
weight: 1
TableOfContents: true
draft: false
---

# What is a Shipping Date used for?
An order's Shipping Date is the date BrewKeeper has calculated the order should be fulfilled (shipped). The _Dispatch Centre_ calendar uses shipping dates when showing which orders should be shipped on each day.
<center>{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shipping-date-calc-dispatch-centre.png" title="Dispatch Centre" width="600px">}}</center>

A Shipping Date is calculated considering the following factors:
* **Fulfillment Days**  
  *Monday through Sunday, which days the Brewery can ship orders*
* **Blackout Dates**  
  *Dates shipping cannot occur, eg. Holidays*
* **Delivery Date**  
  *The preferred date the customer is requesting the shipment be delivered*
* **Lead Days**<sup>1</sup>  
  *The number of _calendar_ days required for a shipping carrier to deliver an order to its destination*
* **Cutoff Time**<sup>2</sup>    
  *Orders created before this time can be shipped the same day*
* **Hold Days**<sup>2</sup>    
  *Number of shipping days required by staff to have the order ready to fulfill*

*<sup>1</sup> for calculating shipping date **with** requested delivery date*<br />
*<sup>2</sup> for calculating shipping date **without** requested delivery date*

Shipping dates can be automatically calculated by BrewKeeper by either back-dating from a customer's requested _delivery date_, or by calculating  forward to the next available shipping date (if no _delivery date_ was requested)

# How a Delivery Date affects the Shipping Date

Shipping date is calculated two different ways. When placing an order, the customer:
1. Does not enter a Delivery Date. This means they want the order as soon as possible
1. Enters a Delivery Date. They want it delivered on a specific date in the future

## Calculating Shipping Date with _no_ Delivery Date

{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shipping-date-calc-order-no-delivery-date.png" title="Order with no requested delivery date" width="400px">}}

BrewKeeper interprets no Delivery Date on an order as meaning "ship the order as soon as possible". The next shipping date is calculated considering:
- **Cutoff time**. If the order is placed before cutoff time, the earliest it *could* be shipped is the same day
- **Hold days**. The number of *shipping days* required to prepare the order for shipment
- **Blackout Dates** and **Fulfillment Days**. BrewKeeper only counts viable _shipping days_ when skipping _hold days_

*Note: if no cutoff time is configured, BrewKeeper assumes a cutoff time of 00:00, which would mean that no order may be shipped on the same day that the order is placed*

*Note: as we are searching for the next available shipping date, the _lead time_ is not considered during this calculation*

<!--
{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shipping-date-calc-hold-time.png" title="Hold time" width="800px">}}
-->
### Example 1: same-day shipping (simple)
- Cutoff time: 8am
- No hold time

Order is created at 7am Monday
- This is before the cutoff time, so it can be shipped the same day, **Monday**

<center>{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shipping-date-calc-order-hold-example1.png" title="" width="800px">}}</center>

### Example 2: 1-day hold time (simple)
- Cutoff time: 8am
- Hold time: 1 day

Order is created at 7am Monday
- This is before the cutoff time, so *could* be prepared and shipped today, HOWEVER
- A hold time of 1 day exists, which gives your team needs at least a full day to prepare
- Order shipping date is **Tuesday**

<center>{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shipping-date-calc-order-hold-example2.png" title="" width="800px">}}</center>

### Example 3: 2-day hold time over blackout days (complex)
- Cutoff time: 8am
- Hold time: 2 days
- Blackout date on the Tuesday and Wednesday

Order is created at 7am Monday
- Since it is before cutoff time, the soonest it could go out is today, HOWEVER
- A hold time of 2 days exists, which states your team needs two **shipping days** to prepare, HOWEVER
- Tuesday and Wednesday are blackout dates, so Monday and Thursday are the 2 hold days required for preparing the shipment
- Shipping day is **Friday**

<center>{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shipping-date-calc-order-hold-example3.png" title="" width="800px">}}</center>

*Note: since the order was created before cutoff, Monday is counted as the first hold day*

## Calculating Shipping Date _with_ a Delivery Date

BrewKeeper interprets a requested Delivery Date on an order as meaning "I don't want this order as soon as possible, I want it on this specific date". This might be because the bar only accepts shipments on certain days of the week.

The shipping date is calculated considering:
- **Lead time**. The minimum number of _calendar_ days it will take the shipping company to ship the goods to the customer
- **Blackout Dates** and **Fulfillment Days**. BrewKeeper ignores viable _shipping days_ when skipping over _lead time_ (as the goods are with the carrier during that time.) BrewKeeper will ensure however that the shipping day always lands on a viable shipping day that is on or before the lead time

*Note: as BrewKeeper is calculating backward from the Delivery Date, we do not consider hold time or order cutoff time in this situation. The order preparation time must be considered by the order management system when allowing users to select a delivery date*

### Example 1: 2 lead days (simple)
- Lead time: 2 days

Order is created with a Delivery Date of **Friday**
- Friday minus 2 lead days is Wednesday
- Shipping day is **Wednesday**

<center>{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shipping-date-calc-order-lead-times.png" title="Lead times" width="800px">}}</center>

### Example 2: avoid shipping on a blackout date (complex)
- Lead time: 2 days

Order is created with a Delivery Date of **Friday**
* Friday minus 2 lead days is Wednesday, which is a Blackout Date _(note that the Thursday blackout date is irrelevant)_
* The closest fulfillment day before Wednesday is **Tuesday**, which is the Shipping Date
<center>{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shipping-date-calc-order-lead-times-with-bo.png" title="Lead times with a Blackout Date" width="800px">}}</center>

# Factors affecting calculating a Shipping Date

## Blackout Dates and Fulfillment Days
When BrewKeeper calculates a Shipping Date, **Blackout Dates** and **Fulfillment Days** are always considered.
* **Blackout Dates** are dates that shipping cannot occur, eg. Holidays
* **Fulfillment Days** are days that orders can be shipped by the brewery. eg. Monday to Friday.  
  *Note: if no Fulfillment Days are set, a Shipping Date cannot be calculated*

In the image below, we see an that Monday to Friday are all Fulfillment Days, and Monday is a Blackout Date. This means that for this week, Monday, Saturday and Sunday cannot be Shipping Dates.

<center>{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shipping-date-calc-fulfillment-blackout-days.png" title="Fulfillment Days and Blackout Dates" width="800px">}}</center>

## How Cutoff Time works
When an order is made, it is assumed that the order cannot be shipped until the following day, unless you specify a Cutoff Time.  
A Cutoff Time means "We can ship the order today, provided it is created on or before the Cutoff Time"  
If a Cutoff Time is not specified, it defaults to the time of 00:00. It means no order will ever be fulfilled on the same day as the order was placed.

Scenario 1:
* No cutoff time set
* Order is created at 1am on Monday
* Shipping Date is **Tuesday**

Scenario 2:
* Cutoff time is set for 8am
* Order is created at 8am on Monday
* Shipping Date is **Monday**

Scenario 3:
* Cutoff time is set for 8am
* Order is created at 8:01am on Monday
* Shipping Date is **Tuesday**

## Lead Times
Lead Times are numbers of days to reach a destination. 

The image below shows an example of how Lead Times are configured. 
<center>{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shipping-date-calc-lead-times.png" title="Lead times" width="800px">}}</center>

Given a Delivery Date, the Shipping Date is calculated backwards. The Lead Time is subtracted (ignoring blackout dates and fulfillment days), and then the closest Shipping Date before that date is found.

The image below shows how a Shipping Date is calculated when given a Delivery Date and there is a Lead Time of 2 days
* The order is created with a Delivery Date of **Friday**
* 2 days Lead Time is subtracted, giving a Shipping Date of **Wednesday**

<center>{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shipping-date-calc-order-lead-times.png" title="Lead times" width="800px">}}</center>

The image below shows how a Shipping Date is calculated when given a Delivery Date and there is a Lead Time of 2 days and a Blackout Date on the intended Shipping Day
* The order is created with a Delivery Date of **Friday**
* 2 days Lead Time is subtracted, giving a possible Shipping Date of **Wednesday**, which is a Blackout Date
* The closest fulfillment day before Wednesday is **Tuesday**, which is the Shipping Date

<center>{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shipping-date-calc-order-lead-times-with-bo.png" title="Lead times with a Blackout Date" width="800px">}}</center>

Notes:
* In the above example, the order will now arrive on Thursday, one day early
* Customers selecting Delivery Dates are usually restricted to a minimum of 5 days or more in the future

## Hold Days

Hold days represents how long your brewery will need to prepare an order if no specific delivery date has been requested by the customer.  
BrewKeeper will skip over a number of otherwise possible shipping days equal to the hold days setting

The image below shows how a Hold Day is applied when there is a non-fulfillment day.
* The order is created on the **Monday**, after cutoff, so the soonest it can be shipped is **Tuesday**
* **Tuesday** is a non-fulfillment day, so it is skipped
* **Wednesday** is a fulfillment day, so the Hold Day is applied
* The order is shipped on **Thursday**

<center>{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shipping-date-calc-add-nf.png" title="Hold Days skipping a non fulfillment day" width="800px">}}</center>

Another example below of how a Hold Day is applied across Blackout Dates and non-fulfillment days.
* The order is created on the **Monday**, after cutoff, so the soonest it can be shipped is **Tuesday**
* **Tuesday** is a non-fulfillment day, so it is skipped
* **Wednesday** is a fulfillment day, but is also a Blackout Date, so it is skipped
* **Thursday** is a fulfillment day, so the Hold Day is applied
* The order is shipped on **Friday**

<center>{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shipping-date-calc-add-bo-and-nf.png" title="Hold Days skipping a blackout date and a non fulfillment day" width="800px">}}</center>

# Recommended Settings

## Local Delivery
- Cutoff time: 8am
- No lead times (always can be delivered within 1 day)

Customers can place orders by 8am to have their order delivered that same day.

## Pickup
- Cutoff time: 8am
- No lead times (makes no sense)

Customers can place orders by 8am to pick up their order the same day from the Brewery.

## B2B Sagawa
- Cutoff time: 8am

**Without a Delivery Date:**  
Customers can place orders by 8am to have their orders possibly shipped that same day.  

**With a Delivery Date:**  
If the customer enters a Delivery Date, the Lead Time will be subtracted from that date to calculate the Shipping Date.  
Consider your Lead Times when setting your Delivery Date restrictions for the customer. eg., If your longest Lead Time is 3 days, the minimum Delivery Date should be at least 5 days to ensure your staff can prepare the order to be shipped in time

## B2C Sagawa
- Cutoff time: 8am
- Hold time: 1

**Without a Delivery Date:**  
Customers who place their orders by 8am can get their order shipped soonest the next day.  
If they miss the cutoff time, the soonest will be in 2 days.

**With a Delivery Date:**  
If the customer enters a Delivery Date, the Lead Time will be subtracted from that date to calculate the Shipping Date.  
Consider your Lead Times when setting your Delivery Date restrictions for the customer. eg., If your longest Lead Time is 3 days, the minimum Delivery Date should be at least 5 days to ensure your staff can prepare the order to be shipped in time
