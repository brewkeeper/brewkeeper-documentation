---
title: 文書
weight: 1
TableOfContents: true
draft: false
---

# 割引

いくつかの無効な割引と有効な割引の例です。数量と合計費用は省略されています。

<span style="color: red;">無効な割引</span>

| 単価 | 割引 | エラー |
|---|---|---|
| 0 | <span style="color: red;">-100</span> | 割引は負になることはできません |
| -100 | <span style="color: red;">1</span> | 単価が負の場合、割引は0でなければなりません |
| 0 | <span style="color: red;">1</span> | 単価が0の場合、割引は0でなければなりません |
| 100 | <span style="color: red;">101</span> | 割引は単価よりも少なくなければなりません |

<span style="color: green;">有効な割引</span>
| 単価 | 割引 | 検証 |
|---|---|---|
| -100 | 0 | 単価が負であり、割引が0である |
| 0 | 0 | 単価が0であり、割引が0である |
| 100 | 1 | 割引が単価よりも少ない |
