---
title: Orders Fulfill Workflow (dep)
weight: 1
TableOfContents: true
draft: true
---

{{< panel status="warning" title="Warning" >}}Being deprecated as of v6.7{{< /panel >}}

**Purpose:** Attach a tracking number to a fulfillment

---

# What is a Shipment?
BrewKeeper now handles order allocations and fulfillments with Shipments.

A Shipment has:
 - tracking number
 - ehiden customer admin number (unique token)
 - shopify fulfillment id
 - shopify order id
 - allocations (made by the iPad)

*Note: An order can have many shipments, though in B2C most of the time will only have 1.*

A Shipment **status** can be:
 - **Shipped** = has a fulfillment id attached
 - **Unshipped** = does not have a fulfillment id attached)

The **Lifecycle** of a Shipment:
 - It is created, and the eHiden token is generated from the OrderID
 - A tracking number is attached (not necessary)
 - Allocations are attached to the shipment
 - Shopify Fulfillment id is attached to the shipment

Depending on the workflow, a Shipment can be created at different times.

---

# Workflows
## A. Create csv -> Allocate -> Import csv -> Fulfill (Expected)

> 1. User **creates shipping slip csv** and loads in eHiden \
\- A Shipment is created\* 
> 2. Logistics **allocates** to order using iPad
> 2. User **imports eHiden csv** to BrewKeeper \
\- BrewKeeper attaches the tracking number to the shipment
> 2. User **fulfills orders** in orders/fulfill \
\- BrewKeeper gets all ipad allocations and attaches them to the Shipments. \
\- BrewKeeper fulfills the orders and attaches (1) the fulfillment ids (2) the tracking numbers

## B. Create csv -> Import csv -> Allocate -> Fulfill
> 1. User **creates shipping slip csv** and loads in eHiden \
\- A Shipment is created\* 
> 2. User **imports eHiden csv** to BrewKeeper \
\- BrewKeeper attaches the tracking number to the shipment
> 2. Logistics **allocate** to order using iPad
> 2. User **fulfills** orders in orders/fulfill \
\- BrewKeeper gets all ipad allocations and attaches them to the Shipments. \
\- BrewKeeper fulfills the orders and attaches (1) the fulfillment ids (2) the tracking numbers

## C. Allocate -> Fulfill
> 1. Logistics **allocate** to order using iPad
> 2. User **fulfills** orders in orders/fulfill \
\- A Shipment is created\*  \
\- BrewKeeper gets all ipad allocations and attaches them to the Shipments. \
\- BrewKeeper fulfills the orders and attaches the fulfillment ids

*Note: \
\*When a Shipment is created, BrewKeeper first looks to see if there are any unshipped Shipments first for these orders. If there are, it will not create a new one.*
