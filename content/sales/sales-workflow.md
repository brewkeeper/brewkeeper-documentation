---
title: Sales Workflows
weight: 1
TableOfContents: true
draft: true
---

As of **v6.6.50**


TODO: 
- Create customer - add `company`
- Fist name, last name is not required


# Overview

Make sure you turn on `Subtitles/Closed Captions`

{{< youtube 9vpXPmGRjkY >}}
 
# Set up

The following instructions guides you through creating a 'Tap Room' Shop.  
This allows you to create orders for the Tap Room, for a Tap Room customer.

## 1. Create a BrewKeeper 'Tap Room' Shop

We don't need to create these orders in an external service, it leaves too much room for error (eg. connection related, shopify related), so we are using a BrewKeeper shop.

**Sales > Settings > Shop Settings > `+ New` > New BrewKeeper Shop**

- Call it 'Tap Room'
- Taxes are inclusive

Next we need to make sure we have products and customers for this shop.

## 2. Create Shop Products / Variants for the new shop

You cannot create orders unless you have shop variants that are for available on the intended shop.

**Sales > Settings > Shop Products > Tap Room > `+ New`**

- Select the product / variants you want to create
- **Update Variant Prices** (fulfilling will FAIL if you don't have prices)
- **Link them**

## 3. Create a Customer for the new shop

**Sales > Customers > `+ New`**

- Fill in the customer details
- Upon save, set the **default address** under **BrewKeeper Settings** on the right, the bottom box  
*Note - NOT default shipping address, just default address*

## 4. Create a Shop Location -> Sales Channel link

This link tells BrewKeeper which sales channel to update (TapRoom) when sales events occur in a Shop Location (Tap Room)  
eg. An order is created in the Tap Room shop location, so according to the link, the TapRoom sales channel should move inventory to Reserved.

**Sales > Settings > Link Shops to Sales Channels > Tap Room | Tap Room > `link`**

- `Tap Room Kegs` Sales Channel should be fine

## 4. Create a new Event Type

**Warehouse > Settings > Event Types > `+ New`**

- Description: **Fulfill to Tap Room**
- Pile from: **Stocked**
- Location from: Wholesale (? will it always come from Wholesale? Perhaps check 'Location From Required')
- Pile to: **Fulfilled**

### 4.a Create Restrictions for this new Event Type

A user should not be able to Fulfill to Tap Room if the keg is currently in:

| Pile (location) | Reason |
|-----------------|--------|
| Unknown         | Should be stocked first |
| Packaged        | Should be stocked first |
| Stocked (ready to ship) | Already allocated to an order |
| Fulfilled | Should be received first |
| Received | Should be packaged stocked |
| Graveyard | Should be recevied first |

**New restrictions:**

Event type: **Fulfill to Tap Room**
1. From pile: **Unknown**  
Message: **not_kegged**
2. From pile: **Packaged**  
Message: **not_stocked**
3. From pile: **Stocked**  
From location: **Ready to Ship**  
Message: **ipad_cannot_fulfill**
4. From pile: **Fulfilled**  
Message: **not_received**
5. From pile: **Received**  
Message: **requires_kegging**  
6. From pile: **Graveyard**  
Message: **not_received**


## 5. Create a Sales Workflow

**Sales > Settings > Sales Workflows > `+ New`**

- Name: **Fulfill to Tap Room**
- Sales Workflow Conditions Event Type: **Fulfill to Tap Room**
- Workflow Action 
  - **Create Order**
  - Customer: **Tap Room(Tap Room)**
  - Shop Location: **Tap Room**
