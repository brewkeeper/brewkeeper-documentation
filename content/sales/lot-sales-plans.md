---
title: "Lot Sales Plans"
weight: 1
TableOfContents: true
draft: true
---

As of **v6.4.12**

-----

# Configuring Lot Sales Plans

Note - This must currently be done by BK staff.

1. Create a default partition. We suggest calling it `Unallocated`.  
1. Create an `InventorySetting` that sets the `default_partition` to the `Unallocated` sales channel  
Why? BrewKeeper needs to know where to remove stock from when you press the **Go!** button
1. Create an `InventorySetting` that sets the `shop_partition` to a Shopify mapped sales channel  
Why? This is the shop BrewKeeper will create orders on
1. Create an `InventorySetting` that sets the `shop`  
Why? This is the shop orders will be created in when you press the **Go!** button  
**note* must be a Shopify shop
