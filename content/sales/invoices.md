---
title: Invoices
weight: 1
TableOfContents: true
draft: true
---

# Monthly Invoices (Consolidated Invoices)
A Montly Invoice is built from Delivery Notes.

# Monthly Invoice Settings

**Sales & Distribution > Settings > Delivery Notes & Invoices > Invoices**

## Order Payment Methods / Payment Terms to include on Consolidated Invoices
You can configure which orders should be included in consolidated invoices based on the orders' `payment methods` or `payment terms`. These filters behave like this:
- if neither is specified, all orders for a billing account will be included in consolidated invoices
- if only one of `payment methods` or `payment terms` is specified, all orders that match any of those criteria will be included in consolidated invoices. The other field will be ignored
- if both are specified, orders that match _either_ criteria will be included in consolidated invoices

## Auto-Archive Delivery Notes & Credit Notes
- Never
- When Delivery Note or Credit Note Approved
- When Invoice Approved

# Questions

## Can an ad-hoc delivery note be added to a Monthly Invoice?

Yes - delivery notes considered for conslidated invoices are:

- Delivery Notes not linked to an order
- Delivery Notes linked to an order with a payment method that matches the "Order payment methods to include on consolidated invoices" settings
  **Sales & Distribution > Settings > Delivery Notes & Invoices > Invoices**

## Can a Monthly Invoice customer get a Delivery Note Invoice or an Invoice each time they receive an order?

No, a Monthly Invoice customer MUST get a Delivery Note. Only Delivery Notes can get added to a Monthly Invoice

## How do I void a consolidated invoice if I'm getting an error that a Delviery Note cannot be unarchived?

Temporarily change the setting "Auto-Archive Delivery Notes & Credit Notes" to Never, void the invoice, then change the setting back.

