---
title: Documents
weight: 1
TableOfContents: true
draft: false
---

# Discounts

<span style="color: green;">Valid discounts</span>
| Unit cost | discount | validation |
|---|---|---|
| -100 | 0 | unit cost is negative, discount is 0 |
| 0 | 0 | unit cost is 0, discount is 0 |
| 100 | 1 | discount is less than unit cost |

# Delivery Note & Invoice Settings

**Sales & Distribution > Settings > Delivery Note & Invoice Settings**

## Sender Details
### Hide Recipient Name 
Controls how the Name1 and Name2 fields are populated.

Eg.
- First Name: Shaun
- Last Name: Cechner
- Company: BrewKeeper

| Hide Recipient Name | Name1 | Name2 |
|---|---|---|
| - | BrewKeeper | Cechner Shaun |
| ✅ | BrewKeeper | |

### Hide Recipient Company

Eg.
- First Name: Shaun
- Last Name: Cechner
- Company: BrewKeeper

| Hide Recipient Company | Name1 | Name2 |
|---|---|---|
| - | BrewKeeper | Cechner Shaun |
| ✅ | Cechner Shaun | |

```ruby
  # for consolidated invoices, this logic will be different:
  # - name1 = company
  # - name2 = last_name first_name
  def self.name_fields(shop, first_name:, last_name:, company:, honorific_title:)
    # 1. In case of name + company, 様 goes on name
    # 2. In case of company only, 御中 goes on company
    # 3. In case of name only, 様 goes on name
    full_name = [last_name, first_name].join(' ').strip

    settings = Setting.current
    # putting peoples' names on B2B orders can be confusing
    hide_name = settings.hide_recipient_name?(shop)
    # putting peoples' companies on B2C orders can be confusing
    hide_company = settings.hide_recipient_company?(shop)

    if company.present? && hide_name
      company += "御中" if honorific_title
      return [company, nil] 
    end

    if full_name.present? && hide_company
      full_name += "様" if honorific_title
      return [full_name, nil] 
    end

    # prefer company / name
    if company.present?
      if full_name.present?
        full_name += "様" if honorific_title
      else
        company += "御中" if honorific_title
      end
      return [company, full_name]
    end

    # fall back on name / nothing
    full_name += "様" if honorific_title && full_name.present?
    return [full_name, nil]
  end
```

