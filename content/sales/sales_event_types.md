---
title: "Sales Events"
weight: 1
TableOfContents: true
draft: true
---

# Sales Event Types #

## Order ##

### order_approved ###

### order_cancelled ###

### order_updated ###

### order_fulfilled ###

### order_closed ###

### fulfillment_created ###

### fulfillment_updated ###

### refund_created ###

### order_refund_restock ###

### order_refund_cancel ###

### order_deleted ###

### shipment_dispatch ###

When you are fulfilling orders for Shopify, a `shipment_dispatch` is created.

1. A `shipment` is built*, it links to an order and a shop, and the shipment is attached to the sales event  \
\*created or found, by a nil fulfillment_id
2. If there is a tracking number, BK currently assumes the user is using `Sagawa (JA)` for shipping, and updates the shipment with that tracking company
3. BK searches for any open allocation on the order (barcodes scanned against line items from the iPad) and attaches them 
4. If there is a line item fee on the order, BK will 'fulfill' the fee line item with this first fulfillment
5. The order's allocations are then fulfilled in Shopify
6. The `shipment_dispatch` sales event is linked to the fulfillment
7. A `fulfillment_created` sales event is created

## Contract ##

### contract_created ###

### contract_updated ###

### contract_deleted ###

### contract_archived ###

### contract_unarchived ###

### contract_approved ###

### contract_cancelled ###

### contract_completed ###

## Shopify ##

### shopify_shop_created ###

### shopify_shop_updated ###

### shopify_shop_deleted ###

### sales_request_committed ###

### sales_request_created ###

### sales_request_cancelled ###

## Other ##

### inventory_sync ###
