---
title: Orders
weight: 1
TableOfContents: true
draft: false
---



<!-- THIS IS OLD STUFF - needs to be revised before adding back in
This page is mainly notes for future implementation.

Shopify orders created in BrewKeeper cannot be updated at all yet.

All you can do to an order in BK is fulfill it, or delete it IF it is a draft order with no external_id.
This is only possible if creating a shopify order failed, leaving a draft brewkeeper order with no external_id.

# Order Workflows
There are rules that guide the workflow of an order. Below is a table which shows what can or cannot be done to an order from different statuses.

{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/orders-lifecycle.png" title="Home Screen" width="600px">}}


|           | Delete | Cancel | Archive | Approve | Updated | Unarchive |
|-----------|:------:|:------:|:-------:|:-------:|:-------:|:---------:|
| Draft     |   ✅    |        |        | ✅     |          |         |
| Approved  |        |   ✅    |         | ✅     |           |         |
| Completed |        |         |  ✅     |       |   ✅       |           |
| Archived  |        |         |         |        | ✅       |   ✅      |
| Cancelled |        |         |  ✅      |         |         |           |
| Deleted   |        |         |         |         |         |           |

# Things that influences orders

- discounts on
  - line items
  - order
  - shipping
- refunds

# Notify settings

**Sales > Settings > Shop Settings** (click on a shop link)

- [ ] Send receipt
  - send an order confirmation to the customer [link](https://shopify.dev/api/admin-rest/2022-10/resources/order)
  - `order_params[:send_receipt]`
- [ ] Send fulfillment receipt
  - send a shipping confirmation to the customer [link](https://shopify.dev/api/admin-rest/2022-10/resources/order)
  - ` order_params[:send_fulfillment_receipt]`
- [ ] Notify customer of fulfillment
  - An email will be sent when the fulfillment is created or updated [link](https://shopify.dev/api/admin-rest/2022-10/resources/fulfillment)
  - `fulfillment_params[:notify_customer]`

-->

# Common Questions


# Shopify Related Questions
## Can I allocate to a line item that does not require shipping?
Yes. 

So what is the point of 'requires shipping'?

The way you set "requires shipping" to false is in Shopify specifying in the variant that it is not a physical product. If this is the case, this variant will not be considered when calculating shipping rates.

