---
title: "Shop Products"
weight: 1
TableOfContents: true
draft: false
---

# What can you do with Shop Products?

A single Product can be sold on multiple shops. There are multiple reasons you would want to do this, including:
* You want to offer different prices for different customers
* Selling Business to Business (B2B) has different tax implications than if you sell Business to Customer (B2C)

BrewKeeper allows you to create a shop product per shop which holds different inventory levels and prices.

In the diagram below, we see a product (IPA 2024) being sold in both a B2B and B2C shop, with key differences:
* The prices are different in each shop
* Single cans are not offered in B2B, and kegs are not offered in B2C

<center>{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shop-products-product-links.png" title="Shop Product links" width="600px">}}</center>

BrewKeeper also lets you easily view and change your inventory levels across all shops.

<center>{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shop-products-sales-channels.png" title="Sales Channels" width="600px">}}</center>


# How to create a new Shop Product
A Shop Product is created from a Product. This ensures less chance of typing errors for the Shop Product and Variant titles, also the Variant SKUS.

<ol start=1><li>Navigate to <b>Sales & Distribution > Shop Products</b></li></ol>
<ol start=2><li>Select Shop</li></ol>

<center>{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shop-products-create-step1&2.png" title="Step 1 and 2" width="600px">}}</center>

<ol start=3><li>Click <b>+ New</b></li></ol>
<center>{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shop-products-create-step3.png" title="Step 3" width="600px">}}</center>

<ol start=4><li>Select the Shop Variants you want to create and click <b>Create</b></li></ol>
<center>{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shop-products-create-step4.png" title="Step 4" width="600px">}}</center>

## Video demonstration
<iframe width="900" height="450" src="https://www.youtube-nocookie.com/embed/TbAEwdHmoGg?si=Io_bS6TysQtRpML3" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

# How to delete a Shop Product

note: this will soon be updated

Currently you must delete a shop product from Shopify.

1. Make sure its unlinked <img>
1. Delete from Shopify <img>

# Questions and Answers
## Shopify related
### How do I change an SKU on a Shop Variant?
Currently, all updates to Shop Variants need to be done in Shopify. 

<ol start=1><li>Click on the Shopify icon on the Shop Product page. This will open the Shop Product in Shopify.</li></ol>
<center>{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shop-products-edit-shopify-variant1.png" title="Step 1" width="600px">}}</center>

<ol start=2><li>(In Shopify) Scroll down until you see the Variants section. Click on the Variant you want to update.</li></ol>
<center>{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shop-products-edit-shopify-variant2.png" title="Step 2" width="600px">}}</center>

<ol start=3><li>(In Shopify) Update your Shop Variant details.</li></ol>
<center>{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/shop-products-edit-shopify-variant3.png" title="Step 2" width="600px">}}</center>


