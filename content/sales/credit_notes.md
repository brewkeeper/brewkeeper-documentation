---
title: "Credit Notes"
weight: 1
TableOfContents: true
---

# What is a Credit Note?
**Credit Notes** are an alternative to **Order Refunds** that can be used if an order has been fulfilled but not yet invoiced.

The details of Credit Notes are incorporated into the next consolidated invoice issued to that Billing Account.

# Set up to create Credit Notes

In BrewKeeper, any customer that prefers monthly invoices (known as **Consolidated Invoices**) can be issued with Credit Notes.

## Set an account to use Consolidated Invoices

1. Open a **billing account**. These can be found in **Accounting > Billing Accounts**
2. Open the **Settings** in the navigation
3. Check “**Prefers consolidated invoices”**

<iframe width="900" height="400" src="https://www.youtube-nocookie.com/embed/x6grWB5S2KU?si=HvXcTOcE4JAp44xD" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

{{% note %}}
The invoicing workflow for Billing Accounts configured to use Consolidated Invoices is different:

- During Shipment Dispatch in the **Dispatch Center** the customer will be issued with a Delivery Note instead of a Delivery Note/Invoice
- Consolidated Invoices will be generated and made available in the _Sales & Distribution -> Invoices_ page under the 'pending' tab at the issue date configured for that billing account. This consolidated invoice will include the details of all Delivery Notes and Credit Notes that were outstanding for that Billing Account at the issue date.
{{% /note %}}

# Create a Draft Credit Note

1. Open an authorised **Delivery Note**
2. On the top right action menu, select “**Create and apply credit**”
3. For each line item, update the quantity you want to credit. Also update the amount you want to credit against shipping
4. Once created, the Draft Credit Note will be shown.


{{% note %}}
* For Consolidated Invoices, it is very important to consider the **ISSUE DATE**, as this date determines which month’s Consolidated Invoice the Credit Note will appear in
{{% /note %}}

<iframe width="900" height="450" src="https://www.youtube-nocookie.com/embed/81pCkMsX0aQ?si=CLsAcFxivqK6Zmiw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

# Authorise a Credit Note

1. With the Credit Note open, click the top right action menu and select “**Authorise**”

{{% note %}}
* If you have the Xero Setting “Send on Authorise”, this is the point where the Credit Note will be sent to Xero and allocated to an Invoice
{{% /note %}}

<iframe width="900" height="450" src="https://www.youtube-nocookie.com/embed/Qe-Qzjd_qr8?si=ocj8lvOZzgdnCOET" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

# Create a Consolidated Invoice with Credit Notes

1. Open the Invoices view. **Sales & Distribution > Invoices**
2. Select the Consolidated Invoice you wish to create that includes the Credit Note and click “**Create Draft**”
3. Open the new Consolidated Invoice

{{% note %}}
- Shipping becomes a line item on Consolidated Invoices
- Credit Notes subtract on Consolidated Invoices
- Draft Credit Notes are not included on Consolidated Invoices
{{% /note %}}

In the video below, you will see:
- 00:10 The Credit Note name in the list of documents that will be included in the Consolidated Invoice
- 00:40 The Credit Note items applied to the Consolidated Invoice with negative (-) amounts

<iframe width="900" height="450" src="https://www.youtube-nocookie.com/embed/18a8CmynMEk?si=xp2oVTRntnrh8wXI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

As shown in the image below, the first item shows that the Invoice includes a Credit Note with the name "CN-00491" which was crediting against Delivery Note "DN-00506". The following item is what was credited.

{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/credit-notes%20-%20consolidated%20invoice%20example.png" title="Credit Note on an Invoice" width="100%">}}

# Add a Credit Note to an existing Consolidated Invoice

If you already have an outstanding Credit Note, you will see a message when looking at the Consolidated Invoice

1. Open the DRAFT Conslidated Invoice
1. Open the **"Referenced Credit Notes"** tab
1. Select the Credit Note(s) you wish to add
1. Click the **"Add"** button

{{% note %}}
- The Credit Note must be AUTHORISED, and the Consolidated Invoice must be DRAFT
- This action will rebuild the Consolidated Invoice, which means **any changes you have made to the Invoice will be lost**
{{% /note %}}

<iframe width="900" height="450" src="https://www.youtube-nocookie.com/embed/Lz7cedai2h8?si=ys3Ef72No9X4zxPt" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

# Remove a Credit Note from a Consolidated Invoice

1. Open the DRAFT Conslidated Invoice
1. Open the **"Referenced Credit Notes"** tab
1. Click the red trash can located at the right side of the Credit Note you wish to remove

{{% note %}}
- The Consolidated Invoice must be Draft
- This action will rebuild the Consolidated Invoice, which means **any changes you have made to the Invoice will be lost**
{{% /note %}}

<iframe width="900" height="450" src="https://www.youtube-nocookie.com/embed/qPVYG4QbEio?si=yZFsRftqc4gmBRZD" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

# Void a Credit Note

1. Open the AUTHORISED credit note
1. Select the right menu options
1. Click **"VOID"**

{{% note %}}
- A Credit Note must be AUTHORISED to be voided
- You can void a credit note if it is on a Consolidated Invoice, whether the Invoice is DRAFT or AUTHORISED
- **Xero** - if this Credit Note has been sent to Xero and there are allocations, the user will receive an error message "Please remove allocations in Xero before voiding"
In this case, the user must log into Xero and manually remove the Credit Note allocations before BrewKeeper will allow let you prodeed
{{% /note %}}

<iframe width="900" height="600" src="https://www.youtube-nocookie.com/embed/rqQ-tdb11h8?si=n0pgJhBy4TkuHIA7" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

# Additional Settings

## Auto-Archive Delivery Notes & Credit Notes

**Sales & Distribution > Settings > Delivery Note & Invoice** in the **Invoices** section.

There is a setting called **Auto-Archive Delivery Notes & Credit Notes** with 3 options

- Never
- When Delivery Note or Credit Note Approved
- When Invoice Approved

If you select (3) "When Invoice Approved", all Delivery Notes and Credit Notes will be archived when you approve the Invoice. If you void the invoice, the Delivery Notes and Credit Notes will then be un-archived.

# Xero Setup

## Send on Authorise

As shown in the screenshot below, you can opt to send Delivery Notes and Credit Notes automatically when they are authorised

This can be found in **Accounting > Xero Settings**

{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/credit-notes-send-on-authorise.png" title="Xero - Send on Authorise" width="600px">}}

# Common Questions

<!-- This block needs to be added to the JP file -->

## Can I create a Credit Note for a customer who does not prefer Consolidated Invoices?

No. Currently the way you issue credit is to add a Credit Note to a Consolidated Invoice. We need to answer the following questions before implementing Credit Notes for Customers that do not get monthly invoices

- When you create a Credit Note for the customer, what would the Credit Note be issued against? How would they receive the credit?
  Would you just create an invoice for this customer, and add the credit note to them? (not implemented)

<!-- END block -->

## Can I void a Delivery Note that has Credit Notes?

You cannot VOID a Delivery Note that has a DRAFT, APPROVED, or ARCHIVED Credit Notes. You must first VOID or DELETE the attached Credit Note(s).

## What is a Consolidated Invoice?

When a customer wants their delivery notes combined into a monthly invoice, we call this a **consolidated invoice**

## Can I create an ad-hoc Credit Note?

BrewKeeper only currently supports issues a Credit Note against a Delivery Note; You are unable to create ad-hoc Credit Notes. 

It is currently assumed that the reason behind credit is because an order or part of an order is returned, and instead of a refund, we are issuing credit. So we credit against the item(s) that are have been returned / thrown away by the customer.

## Can I add new items to a credit note?

You cannot **add** new items OR **remove** **items** from a Credit Note.

You can only **subtract quantity** from each item.

## What happens to discounts on Delivery Notes?

Credit Notes do not have **discounts**. If a delivery note item has a discount, the credit note being built will subtract the discount first

## Why is my Credit Note not adding to my Consolidated Invoice?

A Credit Note must be AUTHORISED. Draft Credit Notes are not added to Consolidated Invoices

## How can I tell if there are outstanding Credit Notes for an Account?

When you open an Invoice, if there are Credit Notes that can be added, you can see a notification at the top of the page

{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/Credit%20Notes%20-%20account%20has%20available%20credit%20notification.png" title="Invoice" width="600px">}}

## Will a Credit Note be automatically archived if I archive a Consolidated Invoice?

This is possible depending on your settings. In these documents see "Auto-Archive Delivery Notes & Credit Notes" under "Additional Settings"

## What happens if I void a Consolidated Invoice that has a Credit Note?

Nothing stops you from VOIDING a Consolidated Invoice that has a Credit Note. This is because the Consolidated Invoice can be re-created.


# Xero Related Questions

## What happens if when you send a Credit Note to Xero, the Delivery Note is not yet sent to Xero?

Before sending Credit Notes to Xero, BrewKeeper first checks the Delivery Notes attached to the Credit Notes have been sent to Xero.

If not, BrewKeeper will inform you which Delivery Notes need to be sent to Xero before you can proceed.

## How do I manually send a Credit Note to Xero?

**Sales & Distribution > Credit Notes > Send to Xero**

Will not show Draft or Deleted Credit Notes

## What happens if I unallocate a Credit Note in Xero?

Xero does not send that information to BrewKeeper, therefore the credit note will still be associated with the delivery note in BrewKeeper

## What happens if i void a Credit Note in Xero?**

Xero does not send BrewKeeper information about Credit Notes. You will need to also void the Credit Note in BrewKeeper if you want your information to be in sync

