---
title: Shipments
weight: 1
TableOfContents: true
draft: false
---

# Prepare Shipment

The list of orders you see include orders:
- with a shipping date of the current date
- that have no shipping code
- that have a shipping code that is not assigned to a carrier

# Questions

## How do I change the document type when an order is added to a shipment?
You can specify the document type that is created per payment method, per carrier. \
This setting is found: \
**Sales & Distribution > Settings > Shipping Carriers**

1. Edit a Shipping Carrier
1. Edit the Document Type to add a payment method

## How do I show prices on delivery notes?
**Sales & Distribution > Settings > Delivery Note & Invoice**

Search for "Show prices on documents"

Check the check boxes for the document types you want to show prices on

- [ ] Delivery Note
- [ ] Delivery Note Invoice
- [ ] Invoice

This can also be set per Payment method

## What are the different document types, and what are their purposes?

- Delivery Note
- Delivery Note Invoice
- Invoice
- Credit Note

A **Delivery Note** is for customers who will pay later. They receive multiple orders throughout the month, each with a Delivery Note detailing what they've received. \
Each month, you create a Monthly Invoice for them which includes all the Delivery Notes they've received, as well as any Credit Notes you've issued for them. The Monthly Invoice will include payment information. \
A Delivery Note does not include payment information.

A **Delivery Note Invoice** is for customers who pay immediately. It includes payment information

An **Invoice** is usually sent to a customer at the end of the month and includes that month's orders. It includes payment information.

A **Credit Note** is issued to a customer in place of a refund. 
