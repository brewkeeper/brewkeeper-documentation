---
title: "クレジットノート"
weight: 1
TableOfContents: true
---

# クレジットノートとは？

クレジットノートは、注文が履行されているがまだ請求書にされていない場合に使用できる、代金の返金手段です。

クレジットノートの利用詳細は、次に発行される請求書に組み込まれます。

# クレジットノートの設定

BrewKeeperでは、月次請求（掛け売り方式）を希望する顧客には、クレジットノートを発行することができます。

## 月次請求書を使用するアカウントの設定

変更したいアカウントページを開きます。これらは「会計 」メニュー> 「請求先」で参照することができます。変更したい顧客アカウントページを選択し、左メニューに表示されている「請求書に関する設定」をクリック。設定画面内にある「月次請求書を希望する」にチェックを入れます。

<iframe width="900" height="400" src="https://www.youtube-nocookie.com/embed/x6grWB5S2KU?si=HvXcTOcE4JAp44xD" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

{{% note %}}
月次請求書を使用するアカウントへの請求フローは異なります：
 
- Dispatch Centerでの出荷発送中に、顧客には納品請求書の代わりに納品書が発行されます。
- 月次請求書は、会計 -> 請求書 ページの ‘保留中(Pending)’ タブに、その請求アカウントごとに設定された発行日に作成され、利用可能になります。この月次請求書には、発行日時点で未決済であるすべての商品代金とクレジットノートが含まれます。
{{% /note %}}

# 下書きのクレジットノートを作成する

1. 発行済みの納品書を開きます。
2. 右上にある新規ボタンで「クレジットの作成」を選択します。
3. 各行のアイテムでクレジットする数量を更新します。また、送料に対してもクレジットする金額を更新します。
4. 作成されると、下書きのクレジットノートが表示されます。

{{% note %}}
* 月次請求書の場合は、発行日が非常に重要です。この日付は、クレジットノートが表示される月次請求書の月を決定する材料になります。
{{% /note %}}

<iframe width="900" height="450" src="https://www.youtube-nocookie.com/embed/81pCkMsX0aQ?si=CLsAcFxivqK6Zmiw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

# クレジットノートの承認

1. クレジットノートを開いた状態で、右上のアクションメニューをクリックし、「承認」を選択します。

{{% note %}}
* Xero設定「承認時に送信する」がある場合、クレジットノートはXeroに送信され、請求書に割り当てられます。
{{% /note %}}

<iframe width="900" height="450" src="https://www.youtube-nocookie.com/embed/Qe-Qzjd_qr8?si=ocj8lvOZzgdnCOET" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

# クレジットノートを含む月次請求書を作成する

1. 請求書ビューを開きます。セールス > 請求書
2. クレジットノートを含む作成したい月次請求書を選択し、「下書きを作成」をクリックす
3. ることで新しい月次請求書が作成されます。


{{% note %}}
- 送料は月次請求書の行項目となります。
- クレジットノートは月次請求書に含まれます。
- 下書きのクレジットノートは月次請求書に含まれません。
{{% /note %}}

次のビデオでは、次のことを説明します：
- 00:10 月次請求書に含まれるドキュメントリストにおけるクレジットノート名
- 00:40 マイナス(-)金額で月次請求書に適用されるクレジットノートアイテム

<iframe width="900" height="450" src="https://www.youtube-nocookie.com/embed/18a8CmynMEk?si=xp2oVTRntnrh8wXI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

以下の画像は、最初のアイテムが「CN-00491」という名前のクレジットノートが含まれることを示しています。それは配送書「DN-00506」にクレジットを行っています。次のアイテムは、クレジットが行われた内容です。

{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/credit-notes%20-%20consolidated%20invoice%20example.png" title="クレジットノートの請求書への追加" width="100%">}}

# 既存の月次請求書にクレジットノートを追加する

既に作成済みのクレジットノートがある場合、月次請求書にメッセージが表示されます。

1. 下書きの月次請求書を開きます。
2. “参照されたクレジットノート” タブを開きます。
3. 追加したいクレジットノートを選択します。
4. “追加” ボタンをクリックします。

{{% note %}}
- クレジットノートは承認されている必要があり、月次請求書は下書きである必要があります。
- このアクションにより、月次請求書が再構成されます。つまり、請求書に行ったすべての変更が失われます。
{{% /note %}}

<iframe width="900" height="450" src="https://www.youtube-nocookie.com/embed/Lz7cedai2h8?si=ys3Ef72No9X4zxPt" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

# 月次請求書からクレジットノートを削除する

1. 下書きの月次請求書を開きます。
2. “参照されたクレジットノート” タブを開きます。
3. 削除したいクレジットノートの右側にある赤いゴミ箱をクリックします。

{{% note %}}
- 月次請求書は下書きである必要があります。
- このアクションにより、月次請求書が再構成されます。つまり、請求書に行ったすべての変更が失われます。
{{% /note %}}

<iframe width="900" height="450" src="https://www.youtube-nocookie.com/embed/qPVYG4QbEio?si=yZFsRftqc4gmBRZD" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

# クレジットノートの無効化

1. 承認済みのクレジットノートを開きます。
1. 左にあるオプションを選択します。
1. “無効化(Void)” をクリックします。

{{% note %}}
- クレジットノートは承認されている必要があります。
- クレジットノートが月次請求書にある場合、請求書が下書きまたは承認済みであっても、無効化できます。
- Xero - クレジットノートがXeroに送信され、割り当てがある場合、ユーザーは「無効化する前にXeroでの割り当てを削除してください」というエラーメッセージを受け取ります。この場合、ユーザーはXeroにログインし、BrewKeeperが続行を許可する前に、クレジットノートの割り当てを手動で削除する必要があります。
{{% /note %}}

<iframe width="900" height="600" src="https://www.youtube-nocookie.com/embed/rqQ-tdb11h8?si=n0pgJhBy4TkuHIA7" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

# 追加設定

## 納品書とクレジットノートの自動アーカイブ

セールス > 設定 > 納品書 & 請求書 の請求書セクションにあります。

納品書とクレジットノートの自動アーカイブという設定があり、3つのオプションがあります。

1　設定しない
2　納品書またはクレジットノートが承認された時
3　請求書が承認された時

(3) の“請求書が承認された時” を選択すると、請求書が承認されるとすべての納品書とクレジットノートがアーカイブされます。請求書を無効化すると、納品書とクレジットノートのアーカイブ状態が解除されます。

# Xeroの設定

## 承認時に送信

以下の画像が示すように、納品書やクレジットノートを承認すると自動的に送信できます。
 
会計 > Xero設定 で確認することができます。

{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/credit-notes-send-on-authorise.png" title="Xero - 承認時に送信" width="600px">}}

# よくある質問

## クレジットノートがある納品書を無効にできますか？

下書き、承認済み、またはアーカイブされたクレジットノートがある納品書を無効にすることはできません。まず、関連するクレジットノートを無効にするか削除する必要があります。

## 月次請求書とは何ですか？

顧客が代金を月ごとにまとめて、支払いたいという要望する場合に、月に一回発行する掛け売り請求書のことを月次請求書と呼びます。

## 納品書なしでクレジットノートを作成できますか？

BrewKeeperは現在、納品書に対してクレジットノートを発行することしかサポートしていません。納品書なしでクレジットノートを作成することはできません。
 
クレジットが発行される理由は、注文または注文の一部が返品された場合のためであり、返金の代わりに次回購入分で相殺できるクレジットを発行していると認識してください。したがって、顧客が返品または破損したアイテムに対してクレジット付与を行います。

## クレジットノートに新しいアイテムを追加できますか？

クレジットノートに新しいアイテムを追加したり、削除したりすることはできません。

各アイテムの数量を減算することしかできません。

## 納品書の割引はどうなりますか？

クレジットノートには割引がありません。納品書のアイテムに割引がある場合、作成されるクレジットノートはまず割引分を差し引きます。

## クレジットノートが月次請求書に追加されないのはなぜですか？

クレジットノートは承認されている必要があります。下書きのクレジットノートは、月次請求書に追加されません。

## アカウントに未処理状態のクレジットノートがあるかどうかをどのように確認できますか？

請求書を開くと、追加できるクレジットノートがある場合には、ページの上部に通知が表示されます。

{{< figure src="https://seshbot-images.s3.amazonaws.com/brewkeeper/docs/Credit%20Notes%20-%20account%20has%20available%20credit%20notification.png" title="請求書" width="600px">}}

## 月次請求書をアーカイブすると、クレジットノートも自動的にアーカイブされますか？

これは設定によって異なります。これらのドキュメントには、追加の設定の下にある「納品書とクレジットノートの自動アーカイブ」を参照してください。

## クレジットノートがある月次請求書を無効にした場合、どうなりますか？

クレジットノートがある月次請求書を無効にすることはできます。これは、月次請求書が再作成できるためです。

# Xero関連の質問

## クレジットノートをXeroに送信する際、納品書がまだXeroに送信されていない場合はどうなりますか？

クレジットノートをXeroに送信する前に、BrewKeeperは最初にクレジットノートに添付された納品書がXeroに送信されているかどうかを確認します。

送信されていない場合、BrewKeeperはXeroに送信する前に送信されていない納品書を通知します。

## クレジットノートを手動でXeroに送信するにはどうすればよいですか？

セールス> クレジットノート > Xeroに送信

このメニューには下書きまたは削除されたクレジットノートは表示されません。

## Xeroでクレジットノートの割り当てを解除するとどうなりますか？

Xeroはその情報をBrewKeeperに送信しないため、クレジットノートは引き続きBrewKeeperの納品書と関連付けられます。

## Xeroでクレジットノートを無効にした場合、どうなりますか？

XeroはBrewKeeperにクレジットノートに関する情報を送信しません。したがって、情報を同期させたい場合は、BrewKeeperでもクレジットノートを無効にする必要があります。
