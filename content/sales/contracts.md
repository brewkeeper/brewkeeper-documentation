---
title: Contracts
weight: 1
TableOfContents: true
---

Introduced **v6.3.35**

**Purpose:** Contracts are useful when you want to:
- Reserve sales inventory
- Record an agreed sale price

Example: Beer Bar XYZ calls and says they would like you to put aside 3x 15 Litre Kegs of your new Pale Ale, and you agree to a discount with them of $100 per keg.

-----

# What is a Contract?

A typical contract can look like this

INSERT A REAL PICTURE OF A CONTRACT HERE

{{% panel title="CONTRACT 001" %}}

 - [X] Reserving (Reserves stock once approved)

| Product | Unit Price | Price Tier Price | Quantity |
|---|---|---|---|
| 6 Pack of IPA | $10 | $10* | 5 | 

\* *Non editable*

{{% /panel %}}

## Reserving

Once approved:
- A Non Reserving contract cannot be changed to a Reserving contract
- You can no longer change quantities on line items, or add new line items
- You can still update unit prices

## Price tiers
This section answers:
- What is a Billing Account?
- What is a Price tier?

### Billing Accounts
A **billing account** (referred to as a Contact in Xero) is a business address. Many customers can have the same Billing Account.

For example:

Company A (billing account)
- Bar 1 (customer)
- Bar 2 (customer)

When you create a **contract**, you create it for the **billing account**.  
When you create an **order**, you create it for the **customer**.

### Price Tier

A billing account can have a **Price tier**. A price tier is used to define additional prices on products.
In the following example, **Wholesale A** and **Wholesale B** are two different price tiers.

Company A (Wholesale A)
- Bar 1
- Bar 2

Company B (Wholesale B)
- Bar 3
- Bar 4

When you create a price tier, you can create variant prices for that price tier.  
Once you assign a Billing Account to this Price Tier, you will see the Price Tier prices on the Contracts you make.

**Assigning a Price Tier**

| Variant | Wholesale A Price | Wholesale B Price |
|---|---|---|
|6 Pack of IPA| $10 | $12 |

**Example Contract line item for Company A**
{{% panel %}}
| Product | Unit Price | Wholesale A Price | Quantity |
|---|---|---|---|
| 6 Pack of IPA |  | $10* |  | 
{{% /panel %}}

**Example Contract line item for Company B**
{{% panel %}}
| Product | Unit Price | Wholesale B Price | Quantity |
|---|---|---|---|
| 6 Pack of IPA |  | $12* |  | 
{{% /panel %}}

-----

# Creating an Order from a Contract
If you have an approved contract for a billing account, the contract will show when you start making an order for a customer

## New Order

**Contract 001**
| Product | Unit Price | Price Tier price | Quantity Remaining | Quantity |
|---|---|---|---|---|
| 6 Pack of IPA | $10 | $10* | 5* | 0 | 

**New Line Item** [+ Select Product] 

<svg width="100" height="30" xmlns="http://www.w3.org/2000/svg">
 <g>
  <title>Layer 1</title>
  <rect rx="10" id="svg_1" height="29.25" width="99" y="0.5" x="0.5" stroke="#000" fill="#007f00"/>
  <text transform="matrix(0.730225, 0, 0, 0.79844, 4.94962, 2.16102)" xml:space="preserve" text-anchor="start" font-family="Noto Sans JP" font-size="24" id="svg_2" y="23.0509" x="21.73888" stroke-width="0" stroke="#000" fill="#ffffff">Approve</text>
 </g>
</svg>

The user can choose a quantity to take from a contract, and edit the unit price before approving.  
The user can also add new line items to the order.

## Order Approve Process

Once an order is approved, various things can happen, whether this is an order created for BK or for Shopify

**Shopify Orders**

1. The quantity is removed from the line item (if taken from a contract line item)
1. A Sales Request is created[^sales-requests-1], creating a Shopify Order, attaching the BrewKeeper Order ID and Sales Request ID as note attributes
1. The Shopify Order is sent back to BrewKeeper via Webhooks
   - BrewKeeper links the Shopify Order (and line items) to the BrewKeeper Order and updates any additional information. It finds the BK order via the attached note attributes previously mentioned
   - BrewKeeper links the Sales Request to the BK Order. It also finds this via the note attributes.
1. BK creates an `order_approved` Sales Event. 
   - If any of the order line items were created from contract line items, BrewKeeper checks to see if the contracts can be completed, and completes them.

**BrewKeeper Orders**

1. The quantity is removed from the line item (if taken from a contract line item)
1. BK creates an `order_approved` Sales Event. 
   - If any of the order line items were created from contract line items, BrewKeeper checks to see if the contracts can be completed, and completes them.


# Auto-archiving a Contract

A contract can be auto-archived upon completion.

1. An order is made
2. The line items of the orders are checked
3. If the order line items are from contracts, the contracts are checked
4. If the contracts of those line items are now totally fulfilled, the contracts are completed
5. If you have **auto-archive** set, the contracts will then be archived

You can set **auto-archive** in Sales -> Settings -> Shop Settings in the **Contracts** subsection.


# Status

A contract can have 1 status at a time, therefore statuses have priority. The priority is written below.

- Deleted
- Archived
- Cancelled
- Completed
- Approved
- Draft

For instance, if you have a draft contract and you approve it, it is now Approved.  
If you then Complete that draft, it is now Completed.  
If you then Archive it, it is now Archived  


There are rules that guide the workflow of a contract. Below is a table which shows what can or cannot be done to a contract from different statuses.

|           | Delete | Cancel | Archive | Complete | Approve | Updated | Unarchive |
|-----------|:------:|:------:|:-------:|:--------:|:-------:|:-------:|:---------:|
| Draft     |   ✅   |        |         |          |    ✅   | ✅     |           | 
| Approved  |        | ✅ [^ca]|        |    ✅    |         | ✅     |           |
| Completed |        |        |  ✅     |          |         | ✅ [^su]|           | 
| Archived  |        |        |         |          |         | ✅ [^su]| ✅       |
| Cancelled |        |        |  ✅     |          |         |         |           |
| Deleted   |        |        |         |          |         |         |           |


[^sales-requests-1]: The sales request does not hold shipping information. It is calculated live again when the sales request is sent to Shopify. What this means, is if you've created an order, and you've been shown the shipping cost is $10, then the sales request fails, then the rates change before you send the sales request again, the resulting order created will have a different shipping rate than the original order.
[^su]: You can update a note on the contract
[^ca]: This will move all reserved stock R -> U

# View remaining contract items for an account

To see which current contracts and a breakdown of the products for an account, navigate to their **Billing Account > Contracts** 

{{< figure src="https://seshbot-images.s3.us-west-2.amazonaws.com/brewkeeper/docs/account-contracts-index.png" title="Billing Account Contracts" width="100%">}}

This page gives you two different views of the contracts for this billing account.

“**Contracts**” tab shows all approved and open contracts with quantity available

{{< figure src="https://seshbot-images.s3.us-west-2.amazonaws.com/brewkeeper/docs/accounting-contrats-index-zoomed.png" title="Billing Account Contracts" width="100%">}}

- `R` means this contract is reserving stock
- Clicking on the contract name will open a preview of the contract

{{< figure src="https://seshbot-images.s3.us-west-2.amazonaws.com/brewkeeper/docs/account-contrats-show-contract.png" title="Billing Account Contract Preview" width="100%">}}

The “**Breakdown”** tab shows you the item names and quantities still available to the account at a quick glance. 

{{< figure src="https://seshbot-images.s3.us-west-2.amazonaws.com/brewkeeper/docs/account-contracts-breakdown-tab.png" title="Billing Account Contracts Breakdown" width="100%">}}

Clicking on the ‘**Copy**’ button will copy and paste this information in a simple format, allowing you to paste them in an email or messenger app. Below is an example of the text given from the above screenshot.

```
Chiaki's KBC Test Contact
CON-75
- 一期一会 新 (DO NOT TOUCH) Eco-Keg - 15.5L x5
- 一期一会 新 (DO NOT TOUCH) Eco-Keg - 20.5L x7
- 一期一会 新 (DO NOT TOUCH) Eco-Keg - 10.4L x9
CON-65
- ハレとケ「ケ」 Can - 350ml x230
CON-49
- 今 (Kon) Can - 350ml 24 Pack x50
CON-48
- ハレとケ「ハレ」 Bottle - 750ml 3 Pack x3
```

{{< youtube rJOz87V9uZU >}}

# Common Questions and Answers

## Do contracts need to reserve inventory?
It does not. When creating a contract, do not check the "Reserves" checkbox

## Can I change a contract from reserving to non-reseving?
Once a contract is approved, you cannot change the quantity, or whether it reserves stock. You also cannot remove or add line items.

## What if I want to add more line items to an approved contract?
Create a new contract. You can create as many contracts as you need.


