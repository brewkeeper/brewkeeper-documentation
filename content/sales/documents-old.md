---
title: Documents
weight: 1
TableOfContents: true
draft: true
---

# Document Service

- Subtotal = line items after discount
- Shipping = shipping after discount
- Total tax = all taxes accumulated
- Total discounts
  - line item discounts
  - *note: shipping discounts are not included* [^shipping-discounts]
- Total
  - Subtotal + Shipping + tax if not tax inclusive

[^shipping-discounts]: To get a shipping discount in shopify, the shopify store must: <br>1. Be a shopify plus store <br>2. Set up a shipping discount script<br>Shopify will then attach both `discounted shipping price` and `undiscounted shipping price` to the order. <br>*note: placing orders in the shopify admin does not attach a discounted shipping price to the order, because the scripts don't touch these orders*

## Order discounts become line items
An order can have line item discounts, and also an order discount.\
BrewKeeper will turn the order discount into a new line item with a -ve amount.

eg.
Order 1 (inclusive)
- line item: $10
- **subtotal**: $10
- order discount: $1
- **tax**: $0.81 (9 * 0.1 / 1.1)
- **total**: $9

Delivery Note / Invoice
- Order 1
- line item $10 (incl. tax $0.91)
- order discount line item $-1 (incl. tax $-0.09)
- **subtotal**: $9
- **tax**: $0.81
- **total**: 9


## Documents with and without tax inclusive orders
You cannot create an invoice or delivery note that has a combination of both tax inclusive and exclusive orders. BrewKeeper will give you give you an error notification.


<div style="display: none;">
  Currently we have a `shipping` value on the document, which is a single value which is the accumulation of all of the orders shipping totals.\
  Shipping is also taxed, and it can also be tax inclusive or exclusive.

  Order 1 (inclusive)
  - line item: $10 (incl. tax $0.91)
  - **subtotal**: $10
  - shipping: $5 (inclu. tax $0.45)
  - **tax**: $1.36
  - **total**: $15

  Order 2 (not inclusive)
  - line item: $10 (excl. tax $1)
  - **subtotal**: $10
  - shipping $5 (excl. tax $0.50)
  - **tax**: $1.50
  - **total**: $16.50 

  Delivery Note / Invoice
  - Order 1
  - line item $10 (incl. tax $0.91)
  - Order 2
  - line item $10 (excl. tax $1)
  - **subtotal**: $20
  - **shipping**: $10.50
  - **tax**: $2.86
    - line items: $1.91 (0.91 + 1)
    - shipping: $0.95 (10.5 * 0.1 / 1.1)
</div>

# Set up

## Document payment methods

**Accounting > Settings**

{{< figure src="https://seshbot-images.s3.us-west-2.amazonaws.com/brewkeeper/docs/document-payment-methods.png" title="Show prices on documents" width="800px">}}

## Show prices per document / payment method

**Accounting > Settings**

{{< figure src="https://seshbot-images.s3.us-west-2.amazonaws.com/brewkeeper/docs/show_prices_on_documents.png" title="Show prices on documents" width="800px">}}
